from django.core.exceptions import ObjectDoesNotExist
from django.shortcuts import render, get_object_or_404
from django.views.decorators.csrf import csrf_protect, csrf_exempt
from rest_framework.views import APIView
from rest_framework.response import Response
from django.http import HttpResponse, JsonResponse
import json
from django.utils.decorators import method_decorator
from rest_framework import status
from rest_framework.generics import CreateAPIView, ListCreateAPIView
from .serializers import ShippingSerializer, PaymentsSerializer, ProductOrderSerializer
from .models import Shipping, ProductOrder, Payments
from drf_yasg.utils import swagger_auto_schema
from drf_yasg import openapi


class ShippingView(APIView):
    def get(self, request, format=None):
        shipping_list = Shipping.objects.all()
        serializer = ShippingSerializer(shipping_list, many=True)
        return Response(serializer.data)

    @swagger_auto_schema(
        request_body=ShippingSerializer,
        # query_serializer=ShippingSerializer,
        responses={
            '200': 'Ok Request',
            '400': "Bad Request"
        },
        security=[],
        operation_id='Create Shipping',
        operation_description='Create of shipping',
    )
    def post(self, request, format=None):
        serializer = ShippingSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            getRate = [
                {'service_name': 'Expedited Parcel', 'price': '19.03'},
                {'service_name': 'Priority', 'price': '38.93'},
                {'service_name': 'Regular Parcel', 'price': '19.03'},
                {'service_name': 'Xpresspost', 'price': '22.83'},

            ]
            resp = {'status': 200, 'shipping_id': serializer.data['id'], 'getRate': getRate}
            return Response(resp, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
