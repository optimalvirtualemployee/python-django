from django.urls import path
from .views import *
from django.views.decorators.csrf import csrf_exempt

app_name = "shipping"

urlpatterns = (
    path("", ShippingView.as_view(), name="shipping"),
)
