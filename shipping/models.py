from django.db import models
from django.conf import settings
from website_admin.models import Product


class Shipping(models.Model):
    country_name = models.CharField(max_length=150)
    user = models.ForeignKey(settings.AUTH_USER_MODEL, blank=True, null=True, on_delete=models.CASCADE,
                                   related_name="user_%(class)ss")
    order_type = models.CharField(max_length=50, null=True, blank=True)
    first_name = models.CharField(max_length=150)
    last_name = models.CharField(max_length=150, null=True, blank=True)
    company_name = models.CharField(max_length=150, blank=True, null=True)
    mail_stop = models.CharField(max_length=150, blank=True, null=True)
    address1 = models.CharField(max_length=255)
    address2 = models.CharField(max_length=255, null=True, blank=True)
    city = models.CharField(max_length=100)
    state = models.CharField(max_length=100)
    zip_code = models.CharField(max_length=100)
    email = models.EmailField(max_length=150)
    phone_number = models.CharField(max_length=50)
    website = models.CharField(max_length=50, null=True, blank=True)
    is_deleted = models.BooleanField(default=0)
    created_at = models.DateTimeField(auto_now_add=True, null=True, blank=True)
    updated_at = models.DateTimeField(auto_now=True, null=True, blank=True)
    updated_by = models.ForeignKey(settings.AUTH_USER_MODEL, blank=True, null=True, on_delete=models.CASCADE,
                                   related_name="updated_%(class)ss")
    created_by = models.ForeignKey(settings.AUTH_USER_MODEL, blank=True, null=True, on_delete=models.CASCADE,
                                   related_name="created_%(class)ss")

    def __str__(self):
        return f"{self.first_name} {self.last_name}"


class ProductOrder(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL, blank=True, null=True, on_delete=models.CASCADE,
                             related_name="user_%(class)ss")
    added_from = models.CharField(max_length=50, null=True, blank=True)
    count = models.IntegerField()
    is_variation = models.BooleanField(default=False)
    productPrice = models.FloatField()
    email = models.EmailField(null=True, blank=True)
    product = models.ForeignKey(Product, blank=True, null=True, on_delete=models.CASCADE,
                                related_name="product_%(class)ss")
    shipping = models.ForeignKey(Shipping, blank=True, null=True, on_delete=models.CASCADE,
                                related_name="shipping_%(class)ss")

    def __str__(self):
        return self.added_from


class Payments(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL, blank=True, null=True, on_delete=models.CASCADE,
                             related_name="user_%(class)ss")
    payment_method = models.CharField(max_length=50)
    bank_po_number = models.CharField(max_length=100)
    email = models.EmailField()
    total_amount = models.FloatField()
    shipping_amt = models.FloatField()
    transaction_id = models.CharField(max_length=100, null=True, blank=True)
    is_approved = models.BooleanField(default=False)
    created_at = models.DateTimeField(auto_now_add=True, null=True, blank=True)
    updated_at = models.DateTimeField(auto_now=True, null=True, blank=True)

    def __str__(self):
        return self.email
