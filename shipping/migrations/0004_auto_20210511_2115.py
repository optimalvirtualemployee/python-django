# Generated by Django 3.1.7 on 2021-05-11 15:45

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('shipping', '0003_auto_20210511_2110'),
    ]

    operations = [
        migrations.AlterField(
            model_name='shipping',
            name='email',
            field=models.EmailField(max_length=150, unique=True),
        ),
    ]
