from rest_framework import serializers
from django.core.mail import send_mail
from core.models import (
    HomeContent,
    Service,
    Client,
    ImageImage,
    HomeSlider,
    Category,
    Banners,
    Logo,
    EmailSubscriber,
    ProductEnquiry,
    ContactEnquiry,
)
from website_admin.models import *


class ImageImageSerializer(serializers.ModelSerializer):
    class Meta:
        model = ImageImage
        fields = "__all__"


class HomeSerializer(serializers.ModelSerializer):
    class Meta:
        model = HomeContent
        fields = "__all__"


class HomeSliderSerializer(serializers.ModelSerializer):
    class Meta:
        model = HomeSlider
        fields = "__all__"


class ServiceSerializer(serializers.ModelSerializer):
    class Meta:
        model = Service
        fields = "__all__"


class ClientSerializer(serializers.ModelSerializer):
    class Meta:
        model = Client
        fields = "__all__"


class CoreCategorySerializer(serializers.ModelSerializer):
    # parent_name = serializers.CharField(read_only=True, source="parent.categoryName")

    class Meta:
        model = Category
        fields = ('id',
                'category_items',
                  'categoryName',
                  'parent_category',
                  'description',
                  'logo',
                  'is_active')


# class SubCategorySerializer(serializers.ModelSerializer):
#     class Meta:
#         model = Category
#         fields = ['id', 'category_items']


class CategorySerializer(serializers.ModelSerializer):
    # child_category = SubCategorySerializer(many=True, read_only=True)

    # def get_child_category(self, obj):
    #     return CategorySerializer(obj.parent).data if obj.parent else None

    class Meta:
        model = Category
        fields = ['id', 'category_items', 'categoryName']


class BannerSerializer(serializers.ModelSerializer):
    class Meta:
        model = Banners
        fields = "__all__"


class LogoSerializer(serializers.ModelSerializer):
    class Meta:
        model = Logo
        fields = "__all__"


class FooterNavbarSerializer(serializers.ModelSerializer):
    class Meta:
        model = FooterNavbar
        exclude = ('menu_type', 'is_deleted', 'created_at', 'updated_at', 'page_name', 'menu_url')
        # fields = '__all__'


class EmailSubscriberSerializer(serializers.ModelSerializer):
    class Meta:
        model = EmailSubscriber
        fields = '__all__'

    def create(self, validate_data):
        instance = super(EmailSubscriberSerializer, self).create(validate_data)
        # send_mail(
           # 'Instance {} has been created'.format(instance.pk),
            #'Here is the message. DATA: {}'.format(validate_data),
            #'rpathak@optimalvirtualemployee.com',
            #[instance.email],
            #fail_silently=False,
        # )
        return instance


class ProductEnquirySerializer(serializers.ModelSerializer):
    class Meta:
        model = ProductEnquiry
        fields = '__all__'

    def create(self, validate_data):
        instance = super(ProductEnquirySerializer, self).create(validate_data)
        #send_mail(
        #    'Instance {} has been created'.format(instance.pk),
        #    'Here is the message. DATA: {}'.format(validate_data),
        #    'rpathak@optimalvirtualemployee.com',
        #    [instance.email],
        #    fail_silently=False,
        #)
        return instance


class ContactEnquirySerializer(serializers.ModelSerializer):
    class Meta:
        model = ContactEnquiry
        fields = '__all__'

    def create(self, validate_data):
        instance = super(ContactEnquirySerializer, self).create(validate_data)
        #send_mail(
        #    'Instance {} has been created'.format(instance.pk),
        #    'Here is the message. DATA: {}'.format(validate_data),
        #    'rpathak@optimalvirtualemployee.com',
        #    [instance.email],
        #    fail_silently=False,
        #)
        return instance


class CurrencySerializer(serializers.ModelSerializer):
    class Meta:
        model = Currency
        fields = ['name', 'conversion_rate', 'icon', 'is_active']


