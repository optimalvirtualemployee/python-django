from django_countries import countries
from django.db.models import Q
from django.conf import settings
from django.core.exceptions import ObjectDoesNotExist
from django.http import Http404
from django.shortcuts import render, get_object_or_404
from django.utils import timezone
from drf_yasg.utils import swagger_auto_schema
from rest_framework.generics import (
    ListAPIView,
    RetrieveAPIView,
    ListCreateAPIView,
    CreateAPIView,
    UpdateAPIView,
    DestroyAPIView,
    RetrieveUpdateDestroyAPIView,
)
from core.permission import *
from rest_framework.permissions import AllowAny, IsAuthenticated
from core.permission import IsOwner
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.status import HTTP_200_OK, HTTP_400_BAD_REQUEST
from .serializers import *
from core.models import HomeContent, Service, Client, Banners, Item
from django.http import HttpResponse, JsonResponse
import json
from customer import serializers as customer_serializer
from website_admin.models import FooterNavbar
from shipping.models import Payments, ProductOrder
from rest_framework import status


class Home(ListAPIView):
    serializer_class = HomeSerializer
    queryset = HomeContent.objects.all()


# class HomeSliderAPIViews(APIView):
#     # serializer_class = HomeSliderSerializer
#     def get(self,request):
#         slider = HomeSlider.objects.all()
#         service = Service.objects.all()
#         sSe = ServiceSerializer(service, many=True)
#         sS = HomeSliderSerializer(slider, many=True)
#         results = {
#             "slider_content":json.loads((JsonResponse(sS.data,safe=False).content)),
#             "service_content":json.loads((JsonResponse(sSe.data,safe=False).content))
#         }
#         return Response(results, status=HTTP_200_OK)


class HomeSliderAPIViews(ListAPIView):
    serializer_class = HomeSliderSerializer
    queryset = HomeSlider.objects.all()


class ServiceView(ListAPIView):
    serializer_class = ServiceSerializer
    queryset = Service.objects.all()


class ClientView(ListAPIView):
    serializer_class = ClientSerializer
    queryset = Client.objects.all()


class BannersView(ListAPIView):
    serializer_class = BannerSerializer
    queryset = Banners.objects.all()


# class CategoryAPIView(ListAPIView):
#     serializer_class = CategorySerializer
#
#     def get_queryset(self):
#         category_item = self.request.query_params.get("category_item", None)
#         # qs = Item.objects.all()
#         queryset = Category.objects.filter(parent=None)
#         if category_item:
#             categoryobj = queryset.filter(category_items=category_item)
#             return categoryobj
#         return queryset

class CategoryAPIView(APIView):
    def get(self, request, format=None):
        """
        Return a list of all Category.
        """
        category_list = list()
        category_item = request.GET.get('category_item', None)
        parent_cat = Category.objects.filter(parent=None)
        if category_item:
            parent_cat = parent_cat.filter(category_items=category_item)
        for parent in parent_cat:
            result = {}
            result['id'] = parent.id
            result['category_items'] = parent.category_items
            result['categoryName'] = parent.categoryName
            result['description'] = parent.description
            result['slug'] = parent.slug
            result['logo'] = str(parent.logo)
            result['is_active'] = parent.is_active
            child_category = []
            for child in Category.objects.filter(parent=parent):
                child_res = {}
                child_res['id'] = child.id
                child_res['category_items'] = child.category_items
                child_res['categoryName'] = child.categoryName
                child_res['description'] = child.description
                child_res['slug'] = parent.slug
                child_res['logo'] = str(child.logo)
                child_res['is_active'] = child.is_active
                child_category.append(child_res)
            result['child'] = child_category
            category_list.append(result)
        return JsonResponse(category_list, safe=False)


class CategoryProductListAPIView(ListAPIView):
    serializer_class = customer_serializer.ProductSerializer

    def get_queryset(self):
        category_item = self.request.query_params.get("category_item", None)
        # qs = Item.objects.all()
        queryset = Product.objects.all()
        if category_item:
            categoryobj = Product.objects.filter(category__category_items=category_item)
            return categoryobj
        return queryset


class LogoView(ListAPIView):
    serializer_class = LogoSerializer
    queryset = Logo.objects.filter(is_active=True)


class FooterMenuView(ListAPIView):
    """
    get all Menu for Footer
    """
    serializer_class = FooterNavbarSerializer
    queryset = FooterNavbar.objects.all()


# class EmailSubscriberView(ListCreateAPIView):
#     serializer_class = EmailSubscriberSerializer
#     queryset = EmailSubscriber.objects.all()


class EmailSubscriberView(APIView):
    """
    Email Subscriber create
    """
    def post(self, request, format=None):
        """
        Create product enquiry and return list of created obj
        """
        if EmailSubscriber.objects.filter(email=self.request.data['email']).exists():
            return Response({"error": "This email id already exists."})
        serializer = EmailSubscriberSerializer(data=request.data)
        try:
            if serializer.is_valid():
                serializer.save()
                return Response(serializer.data, status=status.HTTP_201_CREATED)
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
        except Exception as e:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class ProductEnquiryView(APIView):
    """
    Product Enquiry get and create
    """
    response = dict()
    count = None
    results = None

    def get(self, request, format=None):
        """
            Return a list of all Product Enquiry.
        """
        try:
            queryset = ProductEnquiry.objects.all()
            serializer = ProductEnquirySerializer(queryset, many=True)
            self.count = queryset.count()
            self.results = serializer.data
            self.response['count'] = self.count
            self.response['results'] = self.results
        except Exception as e:
            self.response['count'] = self.count
            self.response['results'] = self.results
        return JsonResponse(self.response)

    def post(self, request, format=None):
        """
        Create product enquiry and return list of created obj
        """
        serializer = ProductEnquirySerializer(data=request.data)
        try:

            if serializer.is_valid():
                serializer.save()
                return Response(serializer.data, status=status.HTTP_201_CREATED)
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
        except Exception as e:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class ContactEnquiryView(ListCreateAPIView):
    serializer_class = ContactEnquirySerializer
    queryset = ContactEnquiry.objects.all()


class CurrencyView(ListCreateAPIView):
    serializer_class = CurrencySerializer
    queryset = Currency.objects.filter(is_deleted=False)


class PaymentView(APIView):
    def post(self, request, format=None):
        data = request.data
        bank_po_number = data.get('bank_po_number')
        user = data.get('user')
        payment_method = data.get('payment_method')
        total_amount = data.get('total_amount')
        email = data.get('email')
        shipping_charge = data.get('shipping_charge')
        shipping_id = data.get('shipping_id')
        product_list = data.get('product_list')
        for prod in product_list:
            added_from = prod.get('added_from')
            count = prod.get('count')
            is_variation = prod.get('is_variation')
            product_price = prod.get('product_price')
            product_id = prod.get('product_id')
            order_obj = ProductOrder()
            order_obj.user = user if user else None
            order_obj.added_from = added_from
            order_obj.count = count
            order_obj.is_variation = is_variation
            order_obj.productPrice = product_price
            order_obj.product_id = product_id if product_id else None
            order_obj.shipping_id = shipping_id if shipping_id else None
            order_obj.save()
        payment_obj = Payments()
        payment_obj.user = user if user else None
        payment_obj.payment_method = payment_method
        payment_obj.bank_po_number = bank_po_number
        payment_obj.email = email
        payment_obj.total_amount = total_amount
        payment_obj.total_amount = total_amount
        payment_obj.shipping_amt = shipping_charge
        payment_obj.save()
        resp = {'status': 200, "message": "Payment successful created"}
        return Response(resp)