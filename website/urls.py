from django.urls import path, include
from .views import *

urlpatterns = [
    path("", Home.as_view(), name="home"),
    path("category/", CategoryAPIView.as_view(), name="category_website"),
    path("category-products/", CategoryProductListAPIView.as_view(), name="home"),
    path("service/", ServiceView.as_view(), name="service"),
    path("client/", ClientView.as_view(), name="client"),
    path("home-slider/", HomeSliderAPIViews.as_view(), name="home-slider"),
    path("banners/", BannersView.as_view(), name="banner"),
    path("logo/", LogoView.as_view(), name="logo"),
    path("email-subscriber/", EmailSubscriberView.as_view(), name="subscriber"),
    path("product-enquiry/", ProductEnquiryView.as_view(), name="product-enquiry"),
    path("currency/", CurrencyView.as_view(), name="currency"),
    path("contact-enquiry/", ContactEnquiryView.as_view(), name="product-enquiry"),
    path("page-manager/", FooterMenuView.as_view(), name="footer_menu"),
    path("payment/", PaymentView.as_view(), name="payment"),
]
