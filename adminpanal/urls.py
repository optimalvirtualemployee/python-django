from django.urls import path, include
from .views import *

urlpatterns = [
    path("user-id/", UserIDView.as_view(), name="user_id"),
    path("add-item/", CreateItemView.as_view(), name="add_item"),
    path("products/", RetrieveItemView.as_view(), name="items"),
    path("item-update/<int:id>/", UpdateItemView.as_view(), name="item_update"),
    path("add-coupon/", CreateCouponView.as_view(), name="add_coupon"),
    path("coupon/", RetrieveCouponView.as_view(), name="coupon"),
    path("deativate/<str:code>/", DeactivateCouponView.as_view(), name="deativate"),
    path("list-category/", ListCategoryView.as_view(), name="list-category"),
    path("create-category/", CreateCategoryView.as_view(), name="create_category"),
    path(
        "update-category/<int:id>/",
        UpdateCategoryView.as_view(),
        name="update_category",
    ),
    path("list-variation/", ViewItemVariation.as_view(), name="list_variation"),
    path("add-variation/", CreateVariation.as_view(), name="add_variation"),
    path(
        "add-item-variation/", CreateItemVariation.as_view(), name="add-item-variation"
    ),
    path("orders/", OrdersView.as_view(), name="orders"),
]
