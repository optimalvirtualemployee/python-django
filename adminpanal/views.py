from django_countries import countries
from django.db.models import Q
from django.conf import settings
from django.core.exceptions import ObjectDoesNotExist
from django.http import Http404
from django.shortcuts import render, get_object_or_404
from django.utils import timezone
from rest_framework.generics import (
    ListAPIView,
    RetrieveAPIView,
    CreateAPIView,
    UpdateAPIView,
    DestroyAPIView,
    RetrieveUpdateDestroyAPIView,
)
from rest_framework import status
from drf_yasg.utils import swagger_auto_schema
from core.permission import *
from rest_framework.permissions import AllowAny, IsAuthenticated
from core.permission import IsOwner
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.status import HTTP_200_OK, HTTP_400_BAD_REQUEST
from core.models import Item, OrderItem, Order
from .serializers import *
from core.models import *


class UserIDView(APIView):
    permission_classes = (IsAuthenticated, IsAdminUser)

    def get(self, request, *args, **kwargs):
        return Response({"userID": request.user.id}, status=HTTP_200_OK)


class RetrieveItemImageView(ListAPIView):
    permission_classes = (IsAuthenticated, IsAdminUser)
    serializer_class = AdminItemImageSerializer
    queryset = ItemImage.objects.all()


class CreateItemView(CreateAPIView):
    # permission_classes = (IsAuthenticated,IsAdminUser)
    serializer_class = AddItemSerializer
    queryset = Item.objects.all()


class RetrieveItemView(ListAPIView):
    permission_classes = (IsAuthenticated, IsAdminUser)
    serializer_class = AdminItemSerializer
    queryset = Item.objects.all()


class UpdateItemView(RetrieveUpdateDestroyAPIView):
    permission_classes = (IsAuthenticated, IsAdminUser)
    serializer_class = AddItemSerializer
    queryset = Item.objects.all()
    lookup_field = "id"

    def get_queryset(self):
        return self.queryset.filter()


class CreateCouponView(CreateAPIView):
    permission_classes = (IsAuthenticated, IsAdminUser)
    serializer_class = AdminCouponSerializer
    queryset = Coupon.objects.all()


class RetrieveCouponView(ListAPIView):
    permission_classes = (IsAuthenticated, IsAdminUser)
    serializer_class = AllCouponSerializer
    queryset = Coupon.objects.all()


class DeactivateCouponView(UpdateAPIView):
    permission_classes = (IsAuthenticated, IsAdminUser)
    serializer_class = DeactivateCouponSerializer
    queryset = Coupon.objects.all()
    lookup_field = "code"

    def get_queryset(self):
        return self.queryset.filter()


class ListCategoryView(ListAPIView):
    permission_classes = (IsAuthenticated, IsAdminUser)
    serializer_class = CategorySerializer
    queryset = Category.objects.all()


class CreateCategoryView(CreateAPIView):
    permission_classes = (IsAuthenticated, IsAdminUser)
    serializer_class = CategorySerializer
    queryset = Category.objects.all()


class UpdateCategoryView(UpdateAPIView):
    permission_classes = (IsAuthenticated, IsAdminUser)
    serializer_class = CategorySerializer
    queryset = Category.objects.all()
    lookup_field = "id"

    def get_queryset(self):
        return self.queryset.filter()


class CreateVariation(CreateAPIView):
    permission_classes = (IsAuthenticated, IsAdminUser)
    serializer_class = VariationSerializer
    queryset = Variation.objects.all()


class ViewItemVariation(ListAPIView):
    permission_classes = (IsAuthenticated, IsAdminUser)
    serializer_class = VariationSerializer
    queryset = Variation.objects.all()


class CreateItemVariation(CreateAPIView):
    permission_classes = (IsAuthenticated, IsAdminUser)
    serializer_class = ItemVariationSerializer
    queryset = ItemVariation.objects.all()


# class OrdersView(ListAPIView):
#     permission_classes = (IsAuthenticated, IsAdminUser)
#     serializer_class = AdminOrderSerializer
#     queryset = Order.objects.all()


class OrdersView(APIView):
    # permission_classes = (IsAuthenticated, IsAdminUser)
    # serializer_class = AdminOrderSerializer
    def get(self, request, format=None):
        order_list = Order.objects.all()
        serializer = AdminOrderSerializer(order_list, many=True)
        return Response(serializer.data)

    @swagger_auto_schema(
        request_body=AdminOrderSerializer,
        # query_serializer=AdminOrderSerializer,
        responses={
            '200': 'Ok Request',
            '400': "Bad Request"
        },
        security=[],
        operation_id='Create Order',
        operation_description='Create of Order',
    )
    def post(self, request, format=None):
        serializer = AdminOrderSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response({'status': 'Order Created'}, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
