from django_countries.serializer_fields import CountryField
from rest_framework import serializers
from core.models import *


class CategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = Category
        fields = (
            "id",
            "categoryName",
            "description",
            "logo",
        )


class ItemImageSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField(required=False)

    class Meta:
        model = ItemImage
        fields = ("id", "item", "image")
        read_only_fields = ("item",)


class AddItemSerializer(serializers.ModelSerializer):
    image = ItemImageSerializer(many=True)

    class Meta:
        model = Item
        fields = (
            "id",
            "productName",
            "productDescription",
            "category",
            "unitPrice",
            "discountPrice",
            "unitsInStock",
            "logo",
            "stl",
            "slug",
            "image",
        )

    def create(self, validated_data):
        image = validated_data.pop("image")
        item = Item.objects.create(**validated_data)
        for img in image:
            ItemImage.objects.create(**img, item=item)
        return item

    def update(self, instance, validated_data):
        image = validated_data.pop("image")
        instance.productName = validated_data.get("productName", instance.productName)
        instance.save()
        keep_image = []
        for img in image:
            print(img)
            if "id" in img.keys():
                if ItemImage.objects.filter(id=img["id"]).exists():
                    i = ItemImage.objects.get(id=img["id"])
                    i.image = img.get("image", i.image)
                    i.save()
                    keep_image.append(i.id)
                else:
                    continue
            else:
                i = ItemImage.objects.create(**img, item=instance)
                keep_image.append(i.id)
        for img in instance.image:
            if img.id not in keep_image:
                img.delete()
        return instance


class AddressSerializer(serializers.ModelSerializer):
    country = CountryField()

    class Meta:
        model = Address
        fields = (
            "id",
            "user",
            "street_address",
            "apartment_address",
            "country",
            "zip",
            "address_type",
            "default_addr",
        )


class AdminCouponSerializer(serializers.ModelSerializer):
    class Meta:
        model = Coupon
        fields = (
            "code",
            "amount",
        )


class AllCouponSerializer(serializers.ModelSerializer):
    class Meta:
        model = Coupon
        fields = (
            "code",
            "amount",
            "is_active",
        )


class DeactivateCouponSerializer(serializers.ModelSerializer):
    class Meta:
        model = Coupon
        fields = ("is_active",)


class AdminItemImageSerializer(serializers.ModelSerializer):
    class Meta:
        model = ItemImage
        fields = ("id", "image")


class ItemVariationSerializer(serializers.ModelSerializer):
    class Meta:
        model = ItemVariation
        fields = ("variation", "value", "attachment")


class VariationSerializer(serializers.ModelSerializer):
    class Meta:
        model = Variation
        fields = ("id", "item", "name", "unitPrise")


class AdminItemSerializer(serializers.ModelSerializer):
    category = serializers.SerializerMethodField()
    images = serializers.SerializerMethodField()

    class Meta:
        model = Item
        fields = (
            "productName",
            "productDescription",
            "category",
            "unitPrice",
            "discountPrice",
            "unitsInStock",
            "logo",
            "stl",
            "slug",
            "images",
        )

    def get_category(self, obj):
        return obj.category.categoryName

    def get_images(self, obj):
        return AdminItemImageSerializer(obj.itemimage_set.all(), many=True).data


class AdminVariationDetailSerializer(serializers.ModelSerializer):
    item = serializers.SerializerMethodField()

    class Meta:
        model = Variation
        fields = ("id", "name", "item")

    def get_item(self, obj):
        return AdminItemSerializer(obj.item).data


class AdminItemVariationDetailSerializer(serializers.ModelSerializer):
    variation = serializers.SerializerMethodField()

    class Meta:
        model = ItemVariation
        fields = ("id", "value", "attachment", "variation")

    def get_variation(self, obj):
        return AdminVariationDetailSerializer(obj.variation).data


class AdminOrderItemSerializer(serializers.ModelSerializer):
    item_variations = serializers.SerializerMethodField()
    item = serializers.SerializerMethodField()
    final_price = serializers.SerializerMethodField()

    class Meta:
        model = OrderItem
        fields = ("id", "item", "item_variations", "quantity", "final_price")

    def get_item(self, obj):
        return AdminItemSerializer(obj.item).data

    def get_item_variations(self, obj):
        return AdminItemVariationDetailSerializer(
            obj.item_variations.all(), many=True
        ).data

    def get_final_price(self, obj):
        return obj.get_final_price()


class AdminOrderSerializer(serializers.ModelSerializer):
    order_items = serializers.SerializerMethodField()
    total = serializers.SerializerMethodField()
    coupon = serializers.SerializerMethodField()

    class Meta:
        model = Order
        # fields = ("id", "order_items", "total", "coupon")
        fields = '__all__'

    def get_order_items(self, obj):
        return AdminOrderItemSerializer(obj.items.all(), many=True).data

    def get_total(self, obj):
        return obj.get_total()

    def get_coupon(self, obj):
        if obj.coupon is not None:
            return AdminCouponSerializer(obj.coupon).data
        return None
