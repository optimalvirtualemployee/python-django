from django.core.files.storage import FileSystemStorage
from django.core.exceptions import ObjectDoesNotExist
from django.shortcuts import render, redirect
from django.http import JsonResponse
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.views import View
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from ..models import *


class ProductView(View):
    template_name = 'products/products.html'
    material_obj = Material.objects.all()
    size_obj = Size.objects.all()
    cat_obj = Category.objects.all()
    context = {
        "products": True,
        'size_obj': size_obj,
        'material_obj': material_obj,
        'cat_obj': cat_obj,
    }

    def get(self, request, *args, **kwargs):
        pk = kwargs.get('pk')
        if pk:
            self.context['title'] = "Edit Product"
            try:
                prod_obj = Product.objects.get(pk=pk)
                prod_img_list = ProductImages.objects.filter(product=prod_obj)
                variation_item_list = ProductItemsVariation.objects.filter(product=prod_obj)
            except ObjectDoesNotExist:
                prod_img_list = None
                prod_obj = None
                variation_item_list = None
        else:
            prod_img_list = None
            prod_obj = None
            variation_item_list = None
            self.context['title'] = "Add Product"
        self.context['prod'] = prod_obj
        self.context['prod_img_list'] = prod_img_list
        self.context['variation_item_list'] = variation_item_list
        return render(request, self.template_name, self.context)

    def post(self, request):
        try:
            p_name = request.POST.get('p_name')
            prod_id = request.POST.get('prod_id', None)
            p_description = request.POST.get('p_description')
            p_categ = request.POST.get('p_categ')
            variation_avail = request.POST.get('variation_avail')
            p_unit_price = request.POST.get('p_unit_price')
            p_discount_price = request.POST.get('p_discount_price')
            p_unit_in_stock = request.POST.get('p_unit_in_stock')
            p_logo = request.FILES.get('p_logo')
            p_images = request.FILES.getlist('p_images[]')
            p_material = request.POST.getlist('p_material[]')
            p_size = request.POST.getlist('p_size[]')
            item_id = request.POST.getlist('item_id[]')
            p_price = request.POST.getlist('p_price[]')
            variant_Stock = request.POST.getlist('variant_Stock[]')
            p_default = request.POST.get('p_default')
            is_shop = request.POST.get('is_shop')
            is_enquiry = request.POST.get('is_enquiry')
            enquiry_text = request.POST.get('enquiry_text')
            if prod_id:
                try:
                    prod_obj = Product.objects.get(id=prod_id)
                except Product.DoesNotExist:
                    prod_obj = Product()
            else:
                prod_obj = Product()

            if p_logo:
                fs = FileSystemStorage()
                filename = fs.save(p_logo.name, p_logo)
                prod_obj.logo = filename
            prod_obj.name = p_name
            prod_obj.description = p_description
            prod_obj.category_id = p_categ
            prod_obj.unitPrice = p_unit_price if p_unit_price else None
            prod_obj.is_shop = True if is_shop else False
            if is_enquiry:
                prod_obj.is_enquiry = True if is_enquiry else False
                prod_obj.enquiry_text = enquiry_text
            prod_obj.discountPrice = p_discount_price if p_discount_price else None
            prod_obj.unitsInStock = p_unit_in_stock
            prod_obj.variation_stat = True if variation_avail else False
            prod_obj.save()

            if p_images:
                for img in p_images:
                    fs = FileSystemStorage()
                    filename = fs.save(img.name, img)
                    prod_img_obj = ProductImages()
                    prod_img_obj.image = filename
                    prod_img_obj.product = prod_obj
                    prod_img_obj.save()

            if variation_avail:
                for i in range(len(p_material)):
                    try:
                        variation_obj = ProductItemsVariation.objects.get(pk=item_id[i])
                    except ProductItemsVariation.DoesNotExist:
                        variation_obj = ProductItemsVariation()
                    if p_material[i] and p_size[i] and p_price[i] and variant_Stock[i]:
                        variation_obj.material_id = p_material[i]
                        variation_obj.size_id = p_size[i]
                        variation_obj.price = p_price[i] if p_price[i] else None
                        variation_obj.variant_Stock = variant_Stock[i] if variant_Stock[i] else None
                        variation_obj.product = prod_obj
                        if p_default:
                            if i == int(p_default):
                                variation_obj.default_material = True
                            else:
                                variation_obj.default_material = False
                    variation_obj.save()
            if prod_id:
                messages.success(request, "Product has been updated successfully!!", extra_tags='alert-success')
            else:
                messages.success(request, "Product has been created successfully!!", extra_tags='alert-success')
            return redirect('admin_product')
        except Exception as e:
            messages.error(request, str(e), extra_tags='alert-danger')
            return redirect('admin_product')

    @staticmethod
    def product_delete(request, pk):
        Product.objects.get(pk=pk).delete()
        messages.success(request, f"Product has been deleted successfully!!", extra_tags='alert-success')
        return redirect("admin_product")

    @staticmethod
    def get_material_and_size(request):
        material_list = list(Material.objects.all().values('id', 'material_name'))
        size_list = list(Size.objects.all().values('id', 'size_name'))
        return JsonResponse({'material_list': material_list, 'size_list': size_list})


@login_required(login_url="admin_login")
def product_list(request):
    if request.user.role == "worker":
        return redirect("worker_index")
    order_list = Product.objects.all().order_by('-id')
    page = request.GET.get("page", 1)
    paginator = Paginator(order_list, 7)
    try:
        order = paginator.page(page)
    except PageNotAnInteger:
        order = paginator.page(1)
    except EmptyPage:
        order = paginator.page(paginator.num_pages)
    context = {"products": True, "order": order}
    return render(request, "products/products_list.html", context)


def products_image_del(request):
    try:
        id = request.GET.get('id')
        data_type = request.GET.get('data_type')
        if data_type == 'main':
            updated = Product.objects.filter(id=id).update(logo=None)
        else:
            updated = ProductImages.objects.filter(id=id).update(image=None)
        if updated:
            return JsonResponse({'status': True})
    except Exception as e:
        return JsonResponse({'status': False})
