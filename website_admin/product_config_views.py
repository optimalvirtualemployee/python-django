from .models import ProductConfig
from django.shortcuts import render
from django.contrib import messages


def product_config(request):
    if request.method == 'POST':
        product_per_page = request.POST.get('product_per_page')
        obj = ProductConfig.objects.create(product_per_page=product_per_page)
        if obj:
            ProductConfig.objects.all().exclude(id=obj.id).delete()
            messages.success(request, "Product config created successfully!!", extra_tags='text-success')
    return render(request, 'product_config.html', {'product_config': True, 'title': 'Product Per Page'})
