from django.shortcuts import render, redirect
from django.contrib import messages
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.template.loader import get_template
from django.template import Context
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from core.models import Order, Item
from .forms import *


def index(request):
    order_list = Item.objects.all()
    page = request.GET.get("page", 1)

    paginator = Paginator(order_list, 7)
    try:
        order = paginator.page(page)
    except PageNotAnInteger:
        order = paginator.page(1)
    except EmptyPage:
        order = paginator.page(paginator.num_pages)
    context = {"dashboard": True, "order": order}
    return render(request, "worker/home.html", context)


@login_required(login_url="admin_login")
def itemsearch(request):
    if request.user.role == "worker":
        return redirect("worker_index")
    filter = request.GET.get("search")
    print(filter)
    order_list = Item.objects.filter(productName__icontains=filter)
    page = request.GET.get("page", 1)
    paginator = Paginator(order_list, 7)
    try:
        order = paginator.page(page)
    except PageNotAnInteger:
        order = paginator.page(1)
    except EmptyPage:
        order = paginator.page(paginator.num_pages)
    context = {"payment": True, "order": reversed(order)}
    return render(request, "worker/products_list.html", context)


def orders(request):
    print(request.user.role)
    return render(request, "worker/orders.html")
