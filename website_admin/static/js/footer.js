$(function() {
  $("#footer_navbar").validate({
    rules: {
      name: "required",
      menu_url: "required",
      menu_title: "required",
    },
    messages: {
      name: "Please enter page name",
      menu_url: "Please enter menu url",
    },
    submitHandler: function(form) {
      form.submit();
    }
  });
});

$(function() {
  $("#currency_form").validate({
    rules: {
      country_name: {
        required: true,
      },
      conversion_rate: {
        required: true,
      },
      currency_name: {
        required: true,
      },
      currency_icon: {
            required: function(element) {
                if(!$('#currency_id').val()) { return true} else { return false }
            }
      },
    },
    messages: {
    },
    submitHandler: function(form) {
      form.submit();
    }
  });
});

CKEDITOR.replace( 'menu_content' );

$(function() {
  $("#product_form").validate({
    rules: {
      p_name: {
        required: true,
      },
      p_description: {
        required: true,
      },
      p_categ: {
        required: true,
      },
//      p_unit_price: {
//        required: true,
//      },
//      p_discount_price: {
//        required: true,
//      },
      "p_size[*]": {
        required: true,
      },
      "p_material[*]": {
        required: true,
      },
      "p_price[*]": {
        required: true,
      },
      "variant_Stock[*]": {
        required: true,
      },
      p_unit_in_stock: {
        required: true,
      },
      p_logo: {
        required: function(element) {
                if(!$('#prod_id').val()) { return true} else { return false }
            }
      },
//      "p_images[]": {
//        required: function(element) {
//                if(!$('#prod_id').val()) { return true} else { return false }
//            }
//      },

    },
    messages: {
    },

  });
});


var x = 1;
var maxField = 100;
$('.add_button').click(function(){
    var fieldHTML = `<div class="row mt-2">
      <div class="col-md-3">
        <input type="hidden" name="item_id[]" value="0">
        <select class="form-control prod_material" name="p_material[]" required>
        </select>
      </div>
      <div class="col-md-3">
        <select class="form-control prod_size" name="p_size[]" required>
        </select>
      </div>
      <div class="col-md-2">
        <input type="number" min="1" placeholder="Variant Stock" name="variant_Stock[]"
                 class="form-control variant_Stock" value="{{ item.price}}" required>
      </div>
      <div class="col-md-2">
        <input type="number" min="1" placeholder="Enter price" name="p_price[]" id="p_price_${x}"
                 class="form-control p_price" required>
      </div>
      <div class="col-md-2">
        <label for="" class="mr-4">
          <input type="radio" value="${x}" name="p_default"> Default</label>
          <a href="javascript:void(0);" class="remove_button text-danger" title="Add field"><i class="fa fa-minus-circle"></i></a>
      </div>
    </div>`;

    $.ajax({
        url: '/admin/get_material_and_size/',
        method: 'GET',
        success: function(res){
            const material_option = [`<option value="">---- Select Material ----</option>`]
            const size_option = [`<option value="">---- Select Size ----</option>`]
            $.each(res.material_list, function(i, item) {
                material_option.push(`<option value="${item.id}">${item.material_name}</option>`)
            });
            $('.prod_material').html(material_option);
            $.each(res.size_list, function(i, item) {
                size_option.push(`<option value="${item.id}">${item.size_name}</option>`)
            });
            $('.prod_size').html(size_option);
        },
    })


    if(x < maxField){
            x++;
            $('.field_wrapper').append(fieldHTML);
        }
});

$('.field_wrapper').on('click', '.remove_button', function(e){
    e.preventDefault();
    $(this).parent().parent('div').remove();
    x--;
});

$(document).on('change', '#variation_avail', function() {
    if($(this).is(":checked")) {
        $('.field_wrapper').show();
    } else {
        $('.field_wrapper').hide();
    }
})

$(document).on('change', '#is_enquiry', function() {
    if($(this).is(":checked")) {
        $('#enquiry_section').show();
    } else {
        $('#enquiry_section').hide();
    }
})

$(document).ready(function() {
    $(document).find('#variation_avail').trigger('change');
})


$(function() {
  $("#size_form").validate({
    rules: {
      size_name: "required",
    },
    messages: {
      name: "Please enter size name",
    },
    submitHandler: function(form) {
      form.submit();
    }
  });
});

$(function() {
  $("#material_form").validate({
    rules: {
      material_name: "required",
    },
    messages: {
      name: "Please enter material name",
    },
    submitHandler: function(form) {
      form.submit();
    }
  });
});

$('#is_footer').change(function(){
    if($(this).is(':checked')) {
        $('#footer_type').show();
    } else {
        $('#footer_type').hide();
    }
});

