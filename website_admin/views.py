from django.core.files.storage import FileSystemStorage
from django.core.exceptions import ObjectDoesNotExist
from django.shortcuts import render, redirect
from django.http import JsonResponse
from django.contrib import messages
from django.db.models import Q
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.template.loader import get_template
from django.template import Context
from django.views import View
import datetime
from django.http import HttpResponse, HttpResponseRedirect
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from core.models import (
    Order,
    Item,
    Banners,
    Website,
    Category,
    ItemImage,
    Variation,
    ItemVariation,

)
from core.permission import IsAdminUser, IsWorkerUser
from .forms import *
from .decorators import *
from .filters import ItemFilter
from authentication import models as auth_models
from django.contrib.auth.forms import UserCreationForm
from django.views.decorators.csrf import csrf_exempt
from django.utils.text import slugify
import uuid
from .models import FooterNavbar, Currency , Material ,Size
from .payment_config_views import *
from .product_config_views import *


def Login(request):
    if request.method == "POST":
        email = request.POST.get("email")
        password = request.POST.get("password")
        user = authenticate(request, email=email, password=password)

        if user is not None:
            login(request, user)
            if request.user.role == "worker":
                return redirect("worker_index")
            return redirect("admin_index")
        else:
            messages.info(request, "Username Or Password Incorrect")
    context = {}
    if request.user.is_authenticated:
        if request.user.role == "worker":
            return redirect("worker_index")
        return redirect("admin_index")
    return render(request, "login.html", context)


@login_required(login_url="admin_login")
def LogoutUser(request):
    logout(request)
    return redirect("admin_login")


@login_required(login_url="admin_login")
def users(request):
    if request.user.role == "worker":
        return redirect("worker_index")
    order_list = auth_models.User.objects.all()
    page = request.GET.get("page", 1)

    paginator = Paginator(order_list, 7)
    try:
        order = paginator.page(page)
    except PageNotAnInteger:
        order = paginator.page(1)
    except EmptyPage:
        order = paginator.page(paginator.num_pages)
    context = {"workers": True, "order": order}
    return render(request, "workers.html", context)


@login_required(login_url="admin_login")
def newusers(request):
    if request.user.role == "worker":
        return redirect("worker_index")
    if request.method == "POST":
        form = SignUpForm(request.POST, request.FILES) or None
        if form.is_valid():
            form.save()
            messages.success(request, "Users has been created successfully!!", extra_tags='text-success')
            return redirect("admin_users")
    else:
        form = SignUpForm()
    context = {"workers": True, "form": form, "title": "New User"}
    return render(request, "inside/form.html", context)


@login_required(login_url="admin_login")
def editusers(request, id):
    if request.user.role == "worker":
        return redirect("worker_index")
    obj = auth_models.User.objects.get(id=id)
    if request.method == "POST":
        form = SignUpForm(request.POST, request.FILES, instance=obj) or None
        if form.is_valid():
            form.save()
            messages.success(request, "Users has been updated successfully!!", extra_tags='text-success')
            return redirect("admin_users")
    else:
        form = SignUpForm(instance=obj)
    context = {"workers": True, "form": form, "title": "Edit User"}
    return render(request, "inside/form.html", context)


@login_required(login_url="admin_login")
def deleteuser(request, id):
    if request.user.role == "worker":
        return redirect("worker_index")
    auth_models.User.objects.get(id=id).delete()
    messages.success(request, "Users has been deleted successfully!!", extra_tags='text-success')
    return redirect("admin_users")


@login_required(login_url="admin_login")
@admin_required
def index(request):
    order_list = Item.objects.all()
    page = request.GET.get("page", 1)

    paginator = Paginator(order_list, 7)
    try:
        order = paginator.page(page)
    except PageNotAnInteger:
        order = paginator.page(1)
    except EmptyPage:
        order = paginator.page(paginator.num_pages)
    context = {"dashboard": True, "order": order}
    return render(request, "dashboard.html", context)


@login_required(login_url="admin_login")
def product1(request):
    if request.user.role == "worker":
        return redirect("worker_index")
    order_list = Item.objects.all()
    page = request.GET.get("page", 1)

    paginator = Paginator(order_list, 7)
    try:
        order = paginator.page(page)
    except PageNotAnInteger:
        order = paginator.page(1)
    except EmptyPage:
        order = paginator.page(paginator.num_pages)
    context = {"products": True, "order": order}
    return render(request, "products_list.html", context)


@login_required(login_url="admin_login")
def logo(request):
    if request.user.role == "worker":
        return redirect("worker_index")
    logo_list = Logo.objects.all().order_by('-id')

    page = request.GET.get("page", 1)

    paginator = Paginator(logo_list, 10)
    try:
        logo_list = paginator.page(page)
    except PageNotAnInteger:
        logo_list = paginator.page(1)
    except EmptyPage:
        logo_list = paginator.page(paginator.num_pages)
    context = {"logo": True, "logo_list": logo_list}
    return render(request, "logo.html", context)


def delete_logo(request, id):
    try:
        Logo.objects.filter(id=id).delete()
        messages.success(request, f"Logo has been deleted successfully!!", extra_tags='text-success')
        return redirect('/admin/logo')
    except Exception as e:
        print(e)


@login_required(login_url="admin_login")
def edit_logo(request, id):
    if request.user.role == "worker":
        return redirect("worker_index")
    obj = Logo.objects.get(id=id)
    if request.method == "POST":
        form = LogoForm(request.POST, request.FILES, instance=obj) or None
        if form.is_valid():
            form.save()
            messages.success(request, "Logo has been updated successfully!!", extra_tags='text-success')
            return redirect("admin_logo")
    else:
        form = LogoForm(instance=obj)
    context = {"menu": True, "form": form, "title": "Edit Logo"}
    return render(request, "inside/form.html", context)


# @csrf_exempt
# def edit_logo(request, id):
#     try:
#         if request.method == "POST":
#             title = request.POST.get('title')
#             description = request.POST.get('description')
#             logo = request.FILES.get('logo')
#             try:
#                 logo_obj = Logo.objects.get(id=id)
#             except ObjectDoesNotExist:
#                 logo_obj = None
#             if logo_obj:
#                 logo_obj.title = title
#                 logo_obj.description = description
#                 if logo:
#                     fs = FileSystemStorage()
#                     filename = fs.save(logo.name, logo)
#                     logo_obj.logo = filename
#                 logo_obj.is_active = True
#                 logo_obj.save()
#                 Logo.objects.filter(is_active=True).exclude(id=id).update(is_active=False)
#                 messages.success(request, f"Logo has been updated successfully!!", extra_tags='text-success')
#                 return HttpResponse({'status': True})
#         logo_obj = Logo.objects.filter(id=id).first()
#         context = {
#             "logo": True,
#             'logo_obj': logo_obj,
#             "title": "Edit Logo",
#         }
#         return render(request, "add_logo.html", context)
#     except Exception as e:
#         print(e)
#         return render(request, "add_logo.html", {"logo": True,  "title": "Edit Logo"})


# @login_required(login_url='admin_login')
@csrf_exempt
def newproduct(request):
    if request.method == "POST":
        data = request.POST
        files = request.FILES
        print(data)

        # form = NewProduct(request.POST,request.FILES) or None
        # if form.is_valid():
        #     form.save()
        slug = ""
        duplicate = Item.objects.filter(slug=slugify(data.get("productName")))
        if duplicate:
            print(duplicate)
            slug = "%s-%s" % (slug, uuid.uuid4().hex[:5])
            print(slug)
        else:
            slug = slugify(data.get("productName"))

        itemobj = Item.objects.create(
            productName=data.get("productName"),
            productDescription=data.get("productDescription"),
            category=Category.objects.get_or_create(categoryName=data.get("category"))[
                0
            ],
            unitPrice=data.get("unitPrice"),
            discountPrice=data.get("discountPrice"),
            unitsInStock=data.get("unitsInStock"),
            logo=files.get("logo"),
            slug=slug,
        )
        img_len = data['img_len']
        for i in range(1, int(img_len) + 1):
            image = "image-" + str(i)
            if files[image]:
                ItemImage.objects.create(item=itemobj, image=files[image])

        variationobjM1 = Variation.objects.create(
            item=itemobj, name=data.get("variationName")
        )
        variationobjM2 = Variation.objects.create(
            item=itemobj, name=data.get("variationQty")
        )

        quantity = data.get("quantity").split(",")
        material = data.get("material").split(",")

        for obj in range(len(material)):
            ItemVariation.objects.create(variation=variationobjM1, value=material[obj])

        for obj in range(len(quantity)):
            ItemVariation.objects.create(variation=variationobjM2, value=quantity[obj])

        return redirect("admin_product")

    context = {"products": True, "title": "New Category"}
    return render(request, "inside/form.html", context)

@csrf_exempt
def editnewproduct(request, id):
    
    if request.method == "POST":
        data = request.POST
        files = request.FILES
        print(data)
        slug = ""
        duplicate = Item.objects.filter(slug=slugify(data.get("productName")))
        if duplicate:
            print(duplicate)
            slug = "%s-%s" % (slug, uuid.uuid4().hex[:5])
            print(slug)
        else:
            slug = slugify(data.get("productName"))

        itemobj = Item.objects.create(
            productName=data.get("productName"),
            productDescription=data.get("productDescription"),
            category=Category.objects.get_or_create(categoryName=data.get("category"))[
                0
            ],
            unitPrice=data.get("unitPrice"),
            discountPrice=data.get("discountPrice"),
            unitsInStock=data.get("unitsInStock"),
            logo=files.get("logo"),
            slug=slug,
        )
        for i in range(1, 5):
            image = "image-" + str(i)
            if files[image]:
                ItemImage.objects.create(item=itemobj, image=files[image])

        variationobjM1 = Variation.objects.create(
            item=itemobj, name=data.get("variationName")
        )
        variationobjM2 = Variation.objects.create(
            item=itemobj, name=data.get("variationQty")
        )

        quantity = data.get("quantity").split(",")
        material = data.get("material").split(",")

        for obj in range(len(material)):
            ItemVariation.objects.create(variation=variationobjM1, value=material[obj])

        for obj in range(len(quantity)):
            ItemVariation.objects.create(variation=variationobjM2, value=quantity[obj])

        return redirect("admin_product")

    else:
        item = Item.objects.get(id = id)
        itemimage = ItemImage.objects.filter(item=item)
        variationsM1 = Variation.objects.filter(item=item)[0]
        variationsM2 = Variation.objects.filter(item=item)[1]
        itemvariationM1 = ItemVariation.objects.filter(variation=variationsM1)
        itemvariationM2 = ItemVariation.objects.filter(variation=variationsM2)
        print(variationsM1,variationsM2,itemvariationM1,itemvariationM2)
    context = {
                "products": True, 
                "title": "New Category", 
                "item": item,
                "itemimage": itemimage,
                "variationsM1": variationsM2,
                "itemvariationM1": itemvariationM1,
                "itemvariationM2": itemvariationM2,
            }
    
    return render(request, "inside/form.html", context)

@login_required(login_url="admin_login")
def editproduct(request, id):
    if request.user.role == "worker":
        return redirect("worker_index")
    obj = Item.objects.get(id=id)
    if request.method == "POST":
        form = NewProduct(request.POST, request.FILES, instance=obj) or None
        if form.is_valid():
            form.save()
            return redirect("admin_product")
    else:
        form = NewProduct(instance=obj)
    context = {"products": True, "form": form, "title": "Edit Product"}
    return render(request, "inside/form.html", context)


@login_required(login_url="admin_login")
def deleteproduct(request, id):
    if request.user.role == "worker":
        return redirect("worker_index")
    Item.objects.get(id=id).delete()
    messages.success(request, f"Product has been deleted successfully")
    return redirect("admin_product")


@login_required(login_url="admin_login")
def productimage(request):
    if request.user.role == "worker":
        return redirect("worker_index")
    order_list = ItemImage.objects.all()
    page = request.GET.get("page", 1)

    paginator = Paginator(order_list, 7)
    try:
        order = paginator.page(page)
    except PageNotAnInteger:
        order = paginator.page(1)
    except EmptyPage:
        order = paginator.page(paginator.num_pages)
    context = {"productimage": True, "order": order}
    return render(request, "productimage.html", context)


@login_required(login_url="admin_login")
def newproductimage(request):
    # if request.user.role == 'worker':
    #             return redirect('worker_index')
    # if request.method == 'POST':
    #     form = ProductImage(request.POST,request.FILES) or None
    #     if form.is_valid():
    #         form.save()
    #         return redirect('admin_product_image')
    # else:
    #     form = ProductImage()
    if request.method == "POST":
        print(request.body)
    context = {
        "products": True,
        # 'form':form,
        "title": "New Product Image",
    }

    return render(request, "add_products.html", context)


@login_required(login_url="admin_login")
def editproductimage(request, id):
    if request.user.role == "worker":
        return redirect("worker_index")
    obj = ItemImage.objects.get(id=id)
    if request.method == "POST":
        form = ProductImage(request.POST, request.FILES, instance=obj) or None
        if form.is_valid():
            form.save()
            return redirect("admin_product_image")
    else:
        form = ProductImage(instance=obj)
    context = {"productimage": True, "form": form, "title": "Edit Product Image"}
    return render(request, "inside/form.html", context)


@login_required(login_url="admin_login")
def deleteproductimage(request, id):
    if request.user.role == "worker":
        return redirect("worker_index")
    ItemImage.objects.get(id=id).delete()
    messages.success(request, f"Your Product has been deleted successfully")
    return redirect("admin_product_image")


@login_required(login_url="admin_login")
def ordersearch(request):
    if request.user.role == "worker":
        return redirect("worker_index")
    filter = request.GET.get("search")
    print(filter)
    order_list = Order.objects.filter(Q(user__email__icontains=filter) | Q(id=filter))
    page = request.GET.get("page", 1)
    paginator = Paginator(order_list, 7)
    try:
        order = paginator.page(page)
    except PageNotAnInteger:
        order = paginator.page(1)
    except EmptyPage:
        order = paginator.page(paginator.num_pages)
    context = {"orders": True, "order": reversed(order)}
    return render(request, "orders.html", context)


@csrf_exempt
def order_tracking(request, pk):
    tracking_id = request.POST.get('tracking_id')
    order_update = Order.objects.filter(pk=pk).update(tracking_id=tracking_id)
    if order_update:
        return JsonResponse({'status': True})
    return JsonResponse({'status': False})


@login_required(login_url="admin_login")
def orders(request):
    if request.user.role == "worker":
        return redirect("worker_index")
    order_list = Order.objects.all()
    page = request.GET.get("page", 1)

    paginator = Paginator(order_list, 7)
    try:
        order = paginator.page(page)
    except PageNotAnInteger:
        order = paginator.page(1)
    except EmptyPage:
        order = paginator.page(paginator.num_pages)
    context = {"orders": True, "order": reversed(order)}
    return render(request, "orders.html", context)


@login_required(login_url="admin_login")
def orderdetails(request, id):
    if request.user.role == "worker":
        return redirect("worker_index")
    orders = Order.objects.get(id=id)
    # orderitem = OrderItem.objects.get(id=orders)
    context = {
        "orders": True,
        "order": orders
        # 'items':orderitem
    }
    return render(request, "orderdetails.html", context)


@login_required(login_url="admin_login")
def couponsearch(request):
    if request.user.role == "worker":
        return redirect("worker_index")
    filter = request.GET.get("search")
    # print(filter)

    order_list = Coupon.objects.filter(Q(code__icontains=filter))
    page = request.GET.get("page", 1)
    paginator = Paginator(order_list, 7)

    try:
        order = paginator.page(page)
    except PageNotAnInteger:
        order = paginator.page(1)
    except EmptyPage:
        order = paginator.page(paginator.num_pages)

    context = {"coupons": True, "order": reversed(order)}
    return render(request, "coupons.html", context)


@login_required(login_url="admin_login")
def newcoupon(request):
    if request.user.role == "worker":
        return redirect("worker_index")
    if request.method == "POST":
        form = CouponForm(request.POST) or None
        if form.is_valid():
            form.save()
            messages.success(request, "Coupons has been created successfully", extra_tags='text-success')
            return redirect("admin_coupons")
    else:
        form = CouponForm()
    context = {"coupons": True, "form": form, "title": "New Coupon"}
    return render(request, "inside/form.html", context)


@login_required(login_url="admin_login")
def coupons(request):
    if request.user.role == "worker":
        return redirect("worker_index")
    order_list = Coupon.objects.all()
    page = request.GET.get("page", 1)

    paginator = Paginator(order_list, 7)
    try:
        order = paginator.page(page)
    except PageNotAnInteger:
        order = paginator.page(1)
    except EmptyPage:
        order = paginator.page(paginator.num_pages)
    context = {"coupons": True, "order": reversed(order)}
    return render(request, "coupons.html", context)


@login_required(login_url="admin_login")
def editcoupon(request, id):
    if request.user.role == "worker":
        return redirect("worker_index")
    obj = Coupon.objects.get(id=id)
    if request.method == "POST":
        form = CouponForm(request.POST, instance=obj) or None
        if form.is_valid():
            form.save()
            messages.success(request, "Coupons has been updated successfully", extra_tags='text-success')
            return redirect("admin_coupons")
    else:
        form = CouponForm(instance=obj)
    context = {"coupons": True, "form": form, "title": "Edit Coupon"}
    return render(request, "inside/form.html", context)


@login_required(login_url="admin_login")
def deletecoupon(request, id):
    if request.user.role == "worker":
        return redirect("worker_index")
    Coupon.objects.get(id=id).delete()
    messages.success(request, f"Coupon has been deleted successfully")
    return redirect("admin_coupons")


@login_required(login_url="admin_login")
def paymentsearch(request):
    if request.user.role == "worker":
        return redirect("worker_index")
    filter = request.GET.get("search")
    # print(filter)

    order_list = Payment.objects.filter(Q(stripe_charge_id__icontains=filter))
    page = request.GET.get("page", 1)
    paginator = Paginator(order_list, 7)

    try:
        order = paginator.page(page)
    except PageNotAnInteger:
        order = paginator.page(1)
    except EmptyPage:
        order = paginator.page(paginator.num_pages)

    context = {"payment": True, "order": reversed(order)}
    return render(request, "payments.html", context)


@login_required(login_url="admin_login")
def payment(request):
    if request.user.role == "worker":
        return redirect("worker_index")
    order_list = Payment.objects.all()
    page = request.GET.get("page", 1)
    query = request.GET.get("search")
    print(query)
    paginator = Paginator(order_list, 7)
    try:
        order = paginator.page(page)
    except PageNotAnInteger:
        order = paginator.page(1)
    except EmptyPage:
        order = paginator.page(paginator.num_pages)
    context = {"payment": True, "order": reversed(order)}
    return render(request, "payments.html", context)


@login_required(login_url="admin_login")
def itemsearch(request):
    if request.user.role == "worker":
        return redirect("worker_index")
    filter = request.GET.get("search")
    print(filter)
    order_list = Item.objects.filter(productName__icontains=filter)
    page = request.GET.get("page", 1)
    paginator = Paginator(order_list, 7)
    try:
        order = paginator.page(page)
    except PageNotAnInteger:
        order = paginator.page(1)
    except EmptyPage:
        order = paginator.page(paginator.num_pages)
    context = {"products": True, "order": reversed(order)}
    return render(request, "products_list.html", context)


@login_required(login_url="admin_login")
def categorysearch(request):
    if request.user.role == "worker":
        return redirect("worker_index")
    filter = request.GET.get("search")
    print(filter)
    order_list = Category.objects.filter(categoryName__icontains=filter)
    page = request.GET.get("page", 1)
    paginator = Paginator(order_list, 7)
    try:
        order = paginator.page(page)
    except PageNotAnInteger:
        order = paginator.page(1)
    except EmptyPage:
        order = paginator.page(paginator.num_pages)
    context = {"category": True, "order": reversed(order)}
    return render(request, "category.html", context)


@login_required(login_url="admin_login")
def category(request):
    if request.user.role == "worker":
        return redirect("worker_index")
    filter = request.GET.get("search")

    if filter:
        order_list = Category.objects.filter(categoryName__icontains=filter).order_by('-id')
        print("df")
    else:
        order_list = Category.objects.all().order_by('-id')

    page = request.GET.get("page", 1)

    paginator = Paginator(order_list, 7)
    try:
        order = paginator.page(page)
    except PageNotAnInteger:
        order = paginator.page(1)
    except EmptyPage:
        order = paginator.page(paginator.num_pages)
    context = {"category": True, "order": order}
    return render(request, "category.html", context)


@login_required(login_url="admin_login")
def newcategory(request):
    if request.user.role == "worker":
        return redirect("worker_index")
    if request.method == "POST":
        form = CategoryForm(request.POST, request.FILES) or None
        if form.is_valid():
            form.save()
            messages.success(request, "Category has been created successfully", extra_tags='text-success')
            return redirect("admin_category")
    else:
        form = CategoryForm()
    context = {"category": True, "form": form, "title": "New Category"}
    return render(request, "inside/form.html", context)


@login_required(login_url="admin_login")
def editcategory(request, id):
    if request.user.role == "worker":
        return redirect("worker_index")
    obj = Category.objects.get(id=id)
    if request.method == "POST":
        form = CategoryForm(request.POST, request.FILES, instance=obj) or None
        if form.is_valid():
            form.save()
            messages.success(request, "Category has been updated successfully", extra_tags='text-success')
            return redirect("admin_category")
    else:
        form = CategoryForm(instance=obj)
    context = {"category": True, "form": form, "title": "Edit Category"}
    return render(request, "inside/form.html", context)


@login_required(login_url="admin_login")
def deletecategory(request, id):
    if request.user.role == "worker":
        return redirect("worker_index")
    Category.objects.get(id=id).delete()
    messages.success(request, "Category has been deleted successfully", extra_tags='text-success')
    return redirect("admin_category")


@login_required(login_url="admin_login")
def variations(request):
    if request.user.role == "worker":
        return redirect("worker_index")
    order_list = Variation.objects.all()
    page = request.GET.get("page", 1)

    paginator = Paginator(order_list, 7)
    try:
        order = paginator.page(page)
    except PageNotAnInteger:
        order = paginator.page(1)
    except EmptyPage:
        order = paginator.page(paginator.num_pages)
    context = {"variations": True, "order": order}
    return render(request, "variations.html", context)


@login_required(login_url="admin_login")
def home(request):
    if request.user.role == "worker":
        return redirect("worker_index")
    order = Service.objects.all()
    home = HomeContent.objects.all()
    sldier = HomeSlider.objects.all()
    banner = Banners.objects.all()
    website = Website.objects.all()
    context = {
        "home_configuration": True,
        "order": order,
        "slider": sldier,
        "website": website,
        "banner": banner,
        "home": home,
    }
    return render(request, "homeconfig.html", context)


@login_required(login_url="admin_login")
def homeconfig(request):
    if request.user.role == "worker":
        return redirect("worker_index")
    order = Service.objects.all()
    home = HomeContent.objects.all()
    sldier = HomeSlider.objects.all()
    banner = Banners.objects.all()
    website = Website.objects.all()
    context = {
        "home_configuration": True,
        "order": order,
        "slider": sldier,
        "website": website,
        "banner": banner,
        "home": home,
    }
    return render(request, "home.html", context)


@login_required(login_url="admin_login")
def sliderconfig(request):
    if request.user.role == "worker":
        return redirect("worker_index")
    order = Service.objects.all()
    home = HomeContent.objects.all()
    sldier = HomeSlider.objects.all().order_by('-id')
    banner = Banners.objects.all()
    website = Website.objects.all()
    context = {
        "home_configuration": True,
        "order": order,
        "slider": sldier,
        "website": website,
        "banner": banner,
        "home": home,
    }
    return render(request, "slider.html", context)


@login_required(login_url="admin_login")
def bannerconfig(request):
    if request.user.role == "worker":
        return redirect("worker_index")
    order = Service.objects.all()
    home = HomeContent.objects.all()
    sldier = HomeSlider.objects.all()
    banner = Banners.objects.all().order_by('-id')
    website = Website.objects.all()
    context = {
        "home_configuration": True,
        "order": order,
        "slider": sldier,
        "website": website,
        "banner": banner,
        "home": home,
    }
    return render(request, "banners.html", context)


@login_required(login_url="admin_login")
def edithome(request, id):
    if request.user.role == "worker":
        return redirect("worker_index")
    obj = HomeContent.objects.get(id=id)
    if request.method == "POST":
        form = HomeContentForm(request.POST, request.FILES, instance=obj) or None
        if form.is_valid():
            form.save()
            return redirect("admin_home_config")
    else:
        form = HomeContentForm(instance=obj)
    context = {"home_configuration": True, "form": form, "title": "Edit Home Content"}
    return render(request, "inside/form.html", context)


@login_required(login_url="admin_login")
def editwebsite(request, id):
    if request.user.role == "worker":
        return redirect("worker_index")
    obj = Website.objects.get(id=id)
    if request.method == "POST":
        form = WebsiteForm(request.POST, request.FILES, instance=obj) or None
        if form.is_valid():
            form.save()
            return redirect("admin_home_config")
    else:
        form = WebsiteForm(instance=obj)
    context = {
        "home_configuration": True,
        "form": form,
        "title": "Edit Website Content",
    }
    return render(request, "inside/form.html", context)


@login_required(login_url="admin_login")
def newservices(request):
    if request.user.role == "worker":
        return redirect("worker_index")
    if request.method == "POST":
        form = ServiceForm(request.POST, request.FILES) or None
        if form.is_valid():
            form.save()
            messages.success(request, "Service has been created successfully!!", extra_tags='text-success')
            return redirect("admin_service")
    else:
        form = ServiceForm()
    context = {"home_configuration": True, "form": form, "title": "New Services"}
    return render(request, "services/add_edit_service.html", context)


@login_required(login_url="admin_login")
def services(request):
    if request.user.role == "worker":
        return redirect("worker_index")
    order = Service.objects.all().order_by('-id')
    context = {"home_configuration": True, "order": order}
    return render(request, "services/service.html", context)


@login_required(login_url="admin_login")
def editservices(request, id):
    if request.user.role == "worker":
        return redirect("worker_index")
    obj = Service.objects.get(id=id)
    if request.method == "POST":
        form = ServiceForm(request.POST, request.FILES, instance=obj) or None
        if form.is_valid():
            form.save()
            messages.success(request, "Service has been updated successfully!!", extra_tags='text-success')
            return redirect("admin_service")
    else:
        form = ServiceForm(instance=obj)
    context = {"home_configuration": True, "form": form, "title": "Edit Service"}
    return render(request, "services/add_edit_service.html", context)


@login_required(login_url="admin_login")
def deleteservices(request, id):
    if request.user.role == "worker":
        return redirect("worker_index")
    Service.objects.get(id=id).delete()
    messages.success(request, "Service has been deleted successfully!!", extra_tags='text-success')
    return redirect("admin_service")


@login_required(login_url="admin_login")
def newslider(request):
    if request.user.role == "worker":
        return redirect("worker_index")
    if request.method == "POST":
        form = SliderForm(request.POST, request.FILES) or None
        if form.is_valid():
            form.save()
            messages.success(request, "Slider has been created successfully!!", extra_tags='text-success')
            return redirect("admin_slider_config")
    else:
        form = SliderForm()
    context = {"home_configuration": True, "form": form, "title": "New Slider"}
    return render(request, "inside/form.html", context)


@login_required(login_url="admin_login")
def editslider(request, id):
    if request.user.role == "worker":
        return redirect("worker_index")
    obj = HomeSlider.objects.get(id=id)
    if request.method == "POST":
        form = SliderForm(request.POST, request.FILES, instance=obj) or None
        if form.is_valid():
            form.save()
            messages.success(request, "Slider has been updated successfully!!", extra_tags='text-success')
            return redirect("admin_slider_config")
    else:
        form = SliderForm(instance=obj)
    context = {"home_configuration": True, "form": form, "title": "Edit Slider"}
    return render(request, "inside/form.html", context)


@login_required(login_url="admin_login")
def deleteslider(request, id):
    if request.user.role == "worker":
        return redirect("worker_index")
    HomeSlider.objects.get(id=id).delete()
    messages.success(request, "Slider has been deleted successfully!!", extra_tags='text-success')
    return redirect("admin_slider_config")


@login_required(login_url="admin_login")
def newbanners(request):
    if request.user.role == "worker":
        return redirect("worker_index")
    if request.method == "POST":
        form = BannersForm(request.POST, request.FILES) or None
        if form.is_valid():
            form.save()
            messages.success(request, "Banner has been created successfully!!", extra_tags='text-success')
            return redirect("admin_banners_config")
    else:
        form = BannersForm()
    context = {"home_configuration": True, "form": form, "title": "New Banner"}
    return render(request, "inside/form.html", context)


@login_required(login_url="admin_login")
def editbanners(request, id):
    if request.user.role == "worker":
        return redirect("worker_index")
    obj = Banners.objects.get(id=id)
    if request.method == "POST":
        form = BannersForm(request.POST, request.FILES, instance=obj) or None
        if form.is_valid():
            form.save()
            messages.success(request, "Banner has been updated successfully!!", extra_tags='text-success')
            return redirect("admin_banners_config")
    else:
        form = BannersForm(instance=obj)
    context = {"home_configuration": True, "form": form, "title": "Edit Banner"}
    return render(request, "inside/form.html", context)


@login_required(login_url="admin_login")
def deletebanners(request, id):
    if request.user.role == "worker":
        return redirect("worker_index")
    Banners.objects.get(id=id).delete()
    messages.success(request, "Banner has been deleted successfully!!", extra_tags='text-success')
    return redirect("admin_banners_config")


@login_required(login_url="admin_login")
def logo(request):
    if request.user.role == "worker":
        return redirect("worker_index")
    logo_list = Logo.objects.all().order_by('-id')

    page = request.GET.get("page", 1)

    paginator = Paginator(logo_list, 10)
    try:
        logo_list = paginator.page(page)
    except PageNotAnInteger:
        logo_list = paginator.page(1)
    except EmptyPage:
        logo_list = paginator.page(paginator.num_pages)
    context = {"logo": True, "logo_list": logo_list}
    return render(request, "logo.html", context)



# @csrf_exempt
# @login_required(login_url="admin_login")
# def addlogo(request):
#     try:
#         if request.method == "POST":
#             title = request.POST.get('title')
#             description = request.POST.get('description')
#             logo = request.FILES.get('logo')
#             fs = FileSystemStorage()
#             filename = fs.save(logo.name, logo)
#             Logo.objects.filter(is_active=True).update(is_active=False)
#             logo_obj = Logo.objects.create(title=title, logo=filename,description=description)
#             logo_obj.save()
#             if logo_obj:
#                 Logo.objects.filter(is_active=True).exclude(id=logo_obj.id).update(is_active=False)
#                 messages.success(request, "Logo has been created successfully!!", extra_tags='text-success')
#                 return HttpResponse({'status': True})
#         context = {
#             "logo": True,
#             # 'form':form,
#             "title": "New Logo",
#             "messages": "Logo has been created successfully",
#         }
#         return render(request, "add_logo.html", context)
#     except Exception:
#         return render(request, "add_logo.html", {"logo": True,  "title": "New Logo"})

@login_required(login_url="admin_login")
def addlogo(request):
    if request.user.role == "worker":
        return redirect("worker_index")
    if request.method == "POST":
        form = LogoForm(request.POST, request.FILES) or None
        if form.is_valid():
            form.save()
            messages.success(request, "Logo has been created successfully!!", extra_tags='text-success')
            return redirect("admin_logo")
    else:
        form = LogoForm()
    context = {"logo": True, "form": form, "title": "New Logo"}
    return render(request, "inside/form.html", context)


def delete_logo(request, id):
    try:
        Logo.objects.filter(id=id).delete()
        messages.success(request, "Logo has been deleted successfully!!", extra_tags='text-success')
        return redirect('/admin/logo')
    except Exception as e:
        print(e)


class FooterView(View):
    template = 'footer_html/add_menu.html'

    def get(self, request, *args, **kwargs):
        footer_type = (('quick_links', 'Quick Links'), ('explore_links', 'Explore Links'))
        if kwargs.get('pk'):
            try:
                menu_obj = FooterNavbar.objects.get(pk=kwargs['pk'])
            except ObjectDoesNotExist:
                menu_obj = None
        else:
            menu_obj = None
        return render(request, self.template, {'menu': True, 'menu_obj': menu_obj, 'footer_type': footer_type})

    def post(self, request):
        try:
            name = request.POST.get('name')
            menu_id = request.POST.get('menu_id')
            menu_url = request.POST.get('menu_url')
            menu_title = request.POST.get('menu_title')
            menu_content = request.POST.get('menu_content')
            is_footer = request.POST.get('is_footer')
            footer_type = request.POST.get('footer_type')
            if menu_id:
                updated = FooterNavbar.objects.filter(id=menu_id).update(
                    menu_title=menu_title,
                    footer_type=footer_type,
                    content=menu_content, is_footer=True if is_footer else False
                )
                messages.success(request, "Menu has been updated successfully!!", extra_tags='text-success')
                return redirect('menu_list')
            if FooterNavbar.objects.filter(menu_title=menu_title).exists():
                messages.success(request, f"{menu_title} already exist!!", extra_tags='text-success')
                return redirect('menu_list')
            obj = FooterNavbar()
            obj.menu_title = menu_title
            obj.content = menu_content
            obj.footer_type = footer_type
            obj.is_footer = True if is_footer else False
            obj.save()
            messages.success(request, "Menu has been created successfully!!", extra_tags='text-success')
            return redirect('menu_list')
        except Exception as e:
            messages.error(request, str(e))
            return render(request, self.template)


def menu_delete(request, pk):
    try:
        FooterNavbar.objects.get(id=pk).delete()
        messages.success(request, "Menu has been deleted successfully!!", extra_tags='text-success')
        return redirect('menu_list')
    except Exception as e:
        messages.error(request, str(e))
        return redirect('menu_list')


def menu_list(request):
    template = 'footer_html/menu_list.html'
    try:
        menu_list = FooterNavbar.objects.all().order_by('-id')

        page = request.GET.get("page", 1)
        paginator = Paginator(menu_list, 10)
        try:
            menu_list = paginator.page(page)
        except PageNotAnInteger:
            menu_list = paginator.page(1)
        except EmptyPage:
            menu_list = paginator.page(paginator.num_pages)
        context = {"menu": True, "menu_list": menu_list}
        return render(request, template, context)
    except Exception as e:
        messages.error(request, str(e))
        return render(request, template)


@login_required(login_url="admin_login")
def contactenquiry(request):
    if request.user.role == "worker":
        return redirect("worker_index")
    contact_list = ContactEnquiry.objects.all().order_by('-id')

    page = request.GET.get("page", 1)

    paginator = Paginator(contact_list, 10)
    try:
        contact_list = paginator.page(page)
    except PageNotAnInteger:
        contact_list = paginator.page(1)
    except EmptyPage:
        contact_list = paginator.page(paginator.num_pages)
    context = {"contactenquiry": True, "contact_list": contact_list}
    return render(request, "contactenquiryreport.html", context)



@login_required(login_url="admin_login")
def emailsubscriberreport(request):
    if request.user.role == "worker":
        return redirect("worker_index")
    email_subsc_list = EmailSubscriber.objects.all().order_by('-id')

    page = request.GET.get("page", 1)

    paginator = Paginator(email_subsc_list, 10)
    try:
        email_subsc_list = paginator.page(page)
    except PageNotAnInteger:
        email_subsc_list = paginator.page(1)
    except EmptyPage:
        email_subsc_list = paginator.page(paginator.num_pages)
    context = {"emailsubscriber": True, "email_subsc_list": email_subsc_list}
    return render(request, "emailsubscriberreport.html", context)


@login_required(login_url="admin_login")
def productenquiryreport(request):
    if request.user.role == "worker":
        return redirect("worker_index")
    product_enquiry_list = ProductEnquiry.objects.all().order_by('-id')

    page = request.GET.get("page", 1)

    paginator = Paginator(product_enquiry_list, 10)
    try:
        product_enquiry_list = paginator.page(page)
    except PageNotAnInteger:
        product_enquiry_list = paginator.page(1)
    except EmptyPage:
        contact_list = paginator.page(paginator.num_pages)
    context = {"product_enquiry_list": True, "product_enquiry_list": product_enquiry_list}
    return render(request, "productenquiryreport.html", context)


class CurrencyView(View):
    template_name = 'currency/currency_add.html'

    def get(self, request, *args, **kwargs):
        pk = kwargs.get('pk')
        country_list = Country.objects.all()
        if pk:
            currency_obj = Currency.objects.get(pk=pk)
        else:
            currency_obj = None
        return render(request, self.template_name, {'currency': currency_obj, 'country_list': country_list})

    def post(self, request):
        try:
            id = request.POST.get('currency_id')
            name = request.POST.get('currency_name')
            conversion_rate = request.POST.get('conversion_rate')
            active = request.POST.get('is_active')
            icon = request.FILES.get('currency_icon')
            country_id = request.POST.get('country_name')
            if id:
                obj = Currency.objects.get(pk=id)
                obj.updated_at = datetime.date.today()
            else:
                obj = Currency()
                obj.created_at = datetime.date.today()
            if icon:
                fs = FileSystemStorage()
                filename = fs.save(icon.name, icon)
                obj.icon = filename
            obj.name = name
            obj.country_id = country_id
            obj.conversion_rate = conversion_rate
            obj.is_active = True if active else False
            obj.save()
            if id:
                messages.success(request, "Currency has been updated successfully")
            else:
                messages.success(request, "Currency has been created successfully")
            return redirect('currency_list')
        except Exception as e:
            messages.error(request, str(e))
            return redirect('currency_list')

    @staticmethod
    def currency_delete(request, pk):
        Currency.objects.filter(pk=pk).update(is_deleted=True)
        messages.success(request, "Currency has been deleted successfully")
        return redirect('currency_list')

    @staticmethod
    def currency_list(request):
        template_name = 'currency/currency_list.html'
        try:
            currency_list_obj = Currency.objects.filter(is_deleted=False).order_by('-id')
            page = request.GET.get("page", 1)

            paginator = Paginator(currency_list_obj, 10)
            try:
                currency_list_obj = paginator.page(page)
            except PageNotAnInteger:
                currency_list_obj = paginator.page(1)
            except EmptyPage:
                currency_list_obj = paginator.page(paginator.num_pages)
            print(currency_list_obj, 'currency_list_obj')

            return render(request, template_name, {'currency_list_obj': currency_list_obj})
        except Exception as e:
            messages.error(request, str(e))
            currency_list_obj = None
            return render(request, template_name, {'currency_list_obj': currency_list_obj})


@login_required(login_url="admin_login")
def deleteemailsubenq(request, id):
    if request.user.role == "worker":
        return redirect("worker_index")
    EmailSubscriber.objects.get(id=id).delete()
    messages.success(request, "EmailEnquiry has been deleted successfully!!", extra_tags='text-success')
    return redirect("admin_emailsubscriber_config")


@login_required(login_url="admin_login")
def deletecontactenq(request, id):
    if request.user.role == "worker":
        return redirect("worker_index")
    ContactEnquiry.objects.get(id=id).delete()
    messages.success(request, "Contact enquiry has been deleted successfully!!", extra_tags='text-success')
    return redirect("admin_contactenq_config")


@login_required(login_url="admin_login")
def deleteproductenq(request, id):
    if request.user.role == "worker":
        return redirect("worker_index")
    ProductEnquiry.objects.get(id=id).delete()
    messages.success(request, "Product enquiry has been deleted successfully!!", extra_tags='text-success')
    return redirect("admin_productenq_config")


@login_required(login_url="admin_login")
def material(request):
    template = 'material/material_list.html'
    try:
        material_list = Material.objects.all().order_by('-id')
        page = request.GET.get("page", 1)
        paginator = Paginator(material_list, 10)
        try:
            material_list = paginator.page(page)
        except PageNotAnInteger:
            material_list = paginator.page(1)
        except EmptyPage:
            material_list = paginator.page(paginator.num_pages)
        context = {"admin_material": True, "material_list": material_list}
        return render(request, template, context)
    except Exception as e:
        messages.error(request, str(e))
        return render(request, template)


@login_required(login_url="admin_login")
def size_list(request):
    template = 'size/size_list.html'
    if request.user.role == "worker":
        return redirect("worker_index")
    size_list = Size.objects.all().order_by('-id')
    page = request.GET.get("page", 1)
    paginator = Paginator(size_list, 10)

    try:
        size_list = paginator.page(page)
    except PageNotAnInteger:
        size_list = paginator.page(1)
    except EmptyPage:
        size_list = paginator.page(paginator.num_pages)

    context = {"Size": True, "size_list": size_list}
    return render(request, template , context)


@login_required(login_url="admin_login")
def new_size(request):
    template = 'size/size_add.html'
    if request.user.role == "worker":
        return redirect("worker_index")
    if request.method == "POST":
        size_name = request.POST.get('size_name')
        size_id = request.POST.get('size_id')
        if size_id:
            Size.objects.filter(pk=size_id).update(size_name=size_name)
            messages.success(request, "Size has been updated successfully!!", extra_tags='text-success')
        else:
            size_obj = Size.objects.create(size_name=size_name)
            size_obj.save()
            messages.success(request, "Size has been created successfully!!", extra_tags='text-success')
        return redirect("admin_list_size")
    context = {"Size": True, "title": "New Size"}
    return render(request, template ,context)


@login_required(login_url="admin_login")
def update_size(request, pk):
    template = 'size/size_add.html'
    try:
        size_obj = Size.objects.get(pk=pk)
    except ObjectDoesNotExist:
        size_obj = None
    context = {"Size": True, 'size_obj': size_obj, "title": "Edit Size"}
    return render(request, template, context)


@login_required(login_url="admin_login")
def size_delete(request, id):
    if request.user.role == "worker":
        return redirect("worker_index")
    Size.objects.get(id=id).delete()
    messages.success(request, "Size has been deleted successfully!!", extra_tags='text-success')
    return redirect("admin_list_size")


@login_required(login_url="admin_login")
def new_material(request):
    template = 'material/material_add.html'
    if request.user.role == "worker":
        return redirect("worker_index")
    if request.method == "POST":
        material_name = request.POST.get('material_name')
        material_id = request.POST.get('material_id')
        if material_id:
            Material.objects.filter(pk=material_id).update(material_name=material_name)
            messages.success(request, "Material has been updated successfully!!", extra_tags='text-success')
        else:
            material_obj = Material.objects.create(material_name=material_name)
            material_obj.save()
            messages.success(request, "Material has been created successfully!!", extra_tags='text-success')
        return redirect("admin_material")
    context = {"admin_material": True, "title": "New Material"}
    return render(request, template ,context)


@login_required(login_url="admin_login")
def update_material(request, pk):
    template = 'material/material_add.html'
    try:
        material_obj = Material.objects.get(pk=pk)
    except ObjectDoesNotExist:
        material_obj = None
    context = {"admin_material": True, 'material_obj': material_obj,  "title": "Edit Material"}
    return render(request, template, context)


@login_required(login_url="admin_login")
def material_delete(request, id):
    if request.user.role == "worker":
        return redirect("worker_index")
    Material.objects.get(id=id).delete()
    messages.success(request, "Material has been deleted successfully!!", extra_tags='text-success')
    return redirect("admin_material")
