from django import forms
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm
from core.models import *
from django.forms import ModelForm
from django.forms import inlineformset_factory
from authentication.models import User
from django.contrib.auth.forms import UserCreationForm
from core import models as core_models
from authentication import models as auth_models
from website_admin.models import Size


class UsersForm(ModelForm):
    class Meta:
        model = User
        fields = "__all__"


class WebsiteForm(ModelForm):
    class Meta:
        model = Website
        fields = "__all__"


class SliderForm(ModelForm):
    class Meta:
        model = HomeSlider
        fields = "__all__"


class BannersForm(ModelForm):
    class Meta:
        model = Banners
        fields = "__all__"


class ProductImage(ModelForm):
    class Meta:
        model = ItemImage
        fields = "__all__"


class NewProduct(ModelForm):
    inlines = [ProductImage]

    class Meta:
        model = Item
        fields = "__all__"


class CategoryForm(ModelForm):
    class Meta:
        model = Category
        # fields = "__all__"
        exclude = ('slug',)

    def __init__(self, *args, **kwargs):
        super(CategoryForm, self).__init__(*args, **kwargs)
        self.fields['parent'].queryset = Category.objects.filter(parent=None).exclude(categoryName=kwargs.get('instance'))


class ServiceForm(ModelForm):
    class Meta:
        model = Service
        fields = "__all__"


class HomeContentForm(ModelForm):
    class Meta:
        model = HomeContent
        fields = "__all__"


class OrderForm(ModelForm):
    class Meta:
        model = Order
        fields = "__all__"


class CouponForm(ModelForm):
    class Meta:
        model = Coupon
        fields = "__all__"


class VariationForm(ModelForm):
    class Meta:
        model = Variation
        fields = "__all__"


class ItemVariationForm(ModelForm):
    class Meta:
        model = ItemVariation
        exclude = ()


ItemVariationForm = inlineformset_factory(
    Variation, ItemVariation, form=ItemVariationForm, extra=1
)


class SignUpForm(UserCreationForm):
    phone_number = forms.IntegerField()
    email = forms.EmailField(max_length=200)
    role = forms.ChoiceField(
        choices=auth_models.role,
        label="",
        initial="",
        widget=forms.Select(),
        required=True,
    )

    class Meta:
        model = User
        fields = ("name", "email", "password1", "password2", "role", "phone_number")


class LogoForm(ModelForm):
    class Meta:
        model = Logo
        fields = "__all__"


class SizeForm(ModelForm):
    class Meta:
        model = Size
        fields = "__all__"


class MaterialForm(ModelForm):
    class Meta:
        model = Size
        fields = "__all__"