from django.db import models
from django.db.models import CharField

from core.models import Country, Category
from django.utils.text import slugify


class FooterNavbar(models.Model):
    page_name = models.CharField(max_length=250, null=True, blank=True)
    menu_title = models.CharField(max_length=250, null=True, blank=True)
    menu_url = models.CharField(max_length=250, null=True, blank=True)
    page_slug = models.SlugField(max_length=100, unique=True)
    content = models.TextField(null=True, blank=True)
    menu_type = models.CharField(max_length=100, null=True, blank=True)
    created_at = models.DateTimeField(auto_now=True, null=True)
    updated_at = models.DateTimeField(auto_now=True, null=True)
    is_deleted = models.BooleanField(default=False)
    is_footer = models.BooleanField(default=False)
    footer_type = models.CharField(max_length=150, null=True, blank=True)

    def __str__(self):
        return self.page_name + '-----' + self.menu_title

    def save(self, *args, **kwargs):
        value = self.menu_title
        self.page_slug = slugify(value, allow_unicode=True)
        super().save(*args, **kwargs)


class Currency(models.Model):
    name = models.CharField(max_length=250, null=True, blank=True)
    icon = models.ImageField(null=True, blank=True)
    convertor = models.FloatField(null=True, blank=True)
    conversion_rate = models.FloatField(default=0.0)
    country = models.ForeignKey(Country, on_delete=models.SET_NULL, related_name="currency_country", null=True, blank=True)
    created_at = models.DateField(null=True, blank=True)
    updated_at = models.DateField(null=True, blank=True)
    is_deleted = models.BooleanField(default=False)
    is_active = models.BooleanField(default=True)

    def __str__(self):
        return self.name


class Product(models.Model):
    name = models.CharField(max_length=255)
    description = models.TextField(null=True, blank=True)
    category = models.ForeignKey(
        Category, related_name="category_%(class)ss", on_delete=models.CASCADE, null=True, blank=True
    )
    unitPrice = models.FloatField(null=True, blank=True)
    is_shop = models.BooleanField(default=True, null=True, blank=True)
    is_enquiry = models.BooleanField(null=True, blank=True)
    discountPrice = models.FloatField(blank=True, null=True)
    unitsInStock = models.IntegerField(null=True, blank=True)
    logo = models.ImageField(blank=True, null=True, upload_to="product")
    # stl = models.FileField(blank=True, null=True)
    variation_stat = models.BooleanField(default=False)
    enquiry_text = models.CharField(blank=True, null=True, max_length=255)
    slug = models.SlugField(max_length=255, null=True, blank=True, unique=True)
    created_at = models.DateTimeField(auto_now_add=True, null=True, blank=True)
    created_at = models.DateTimeField(auto_now=True, null=True, blank=True)

    def __str__(self):
        return self.name

    def save(self, *args, **kwargs):
        self.slug = slugify(self.name, allow_unicode=True)
        super(Product, self).save(*args, **kwargs)


class ProductImages(models.Model):
    product = models.ForeignKey(Product, related_name="product_%(class)ss",
                                on_delete=models.CASCADE, null=True, blank=True)
    image = models.ImageField(null=True, blank=True, upload_to="product")

    def __str__(self):
        return self.product.name


class ProductVariation(models.Model):
    product = models.ForeignKey(Product, related_name="product_%(class)ss",
                                on_delete=models.CASCADE, null=True, blank=True)
    name = models.CharField(max_length=250, null=True, blank=True)
    quantity = models.IntegerField(blank=True, null=True)

    def __str__(self):
        return self.name


class Material(models.Model):
    material_name = models.CharField(max_length=250, null=True, blank=True)
    created_at = models.DateTimeField(auto_now=True, null=True)
    updated_at = models.DateTimeField(auto_now=True, null=True)
    is_status = models.BooleanField(default=False)

    def __str__(self):
        return self.material_name


class Size(models.Model):
    size_name = models.CharField(max_length=250, null=True, blank=True)
    created_at = models.DateTimeField(auto_now=True, null=True)
    updated_at = models.DateTimeField(auto_now=True, null=True)
    is_status =  models.BooleanField(default=True)

    def __str__(self):
        return self.size_name


class ProductItemsVariation(models.Model):
    product = models.ForeignKey(Product, related_name="product_%(class)ss",
                                on_delete=models.CASCADE, null=True, blank=True)
    prod_variation = models.ForeignKey(ProductVariation, related_name="prod_variation_%(class)ss",
                                on_delete=models.CASCADE, null=True, blank=True)
    material = models.ForeignKey(Material, null=True, blank=True, on_delete=models.CASCADE,
                                 related_name="material_%(class)ss")
    size = models.ForeignKey(Size, null=True, blank=True, on_delete=models.CASCADE,
                             related_name="size_%(class)ss")
    price = models.IntegerField(blank=True, null=True)
    variant_Stock = models.IntegerField(blank=True, null=True)
    default_material = models.BooleanField(blank=True, null=True, default=False)

    def __str__(self):
        return self.material.material_name


class ApiConfig(models.Model):
    CONFIG_TYPE = ((0, 'Sandbox'), (1, 'Production'))
    config_type = models.IntegerField(default=0, choices=CONFIG_TYPE)
    api_key = models.CharField(max_length=255)
    secret_key = models.CharField(max_length=255)

    def __str__(self):
        return self.api_key


class ProductConfig(models.Model):
    product_per_page = models.IntegerField()

    def __str__(self):
        return str(self.product_per_page)





