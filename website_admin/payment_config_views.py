from .models import ApiConfig
from django.shortcuts import render
from django.contrib import messages


def payment_config(request):
    if request.method == 'POST':
        api_config_type = request.POST.get('api_config_type')
        api_key = request.POST.get('api_key')
        secret_key = request.POST.get('secret_key')
        api_obj = ApiConfig.objects.create(config_type=api_config_type, api_key=api_key, secret_key=secret_key)
        if api_obj:
            ApiConfig.objects.all().exclude(id=api_obj.id).delete()
            messages.success(request, "Payment config created successfully!!", extra_tags='text-success')
    return render(request, 'api_config.html', {'payment_config': True, 'title': 'Payment Configuration'})
