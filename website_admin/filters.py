from core import models as core_models
import django_filters


class ItemFilter(django_filters.FilterSet):
    class Meta:
        model = core_models.Item
        fields = ["productName", "id"]
