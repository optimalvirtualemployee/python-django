# Generated by Django 3.1.7 on 2021-04-04 07:45

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0036_country'),
        ('website_admin', '0007_auto_20210327_0943'),
    ]

    operations = [
        migrations.AddField(
            model_name='currency',
            name='conversion_rate',
            field=models.FloatField(default=0.0),
        ),
        migrations.AddField(
            model_name='currency',
            name='country',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='currency_country', to='core.country'),
        ),
    ]
