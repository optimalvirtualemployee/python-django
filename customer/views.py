from django_countries import countries
from django.db.models import Q
import json
from django.conf import settings
from django.core.exceptions import ObjectDoesNotExist
from django.http import Http404, JsonResponse, HttpResponse
from django.shortcuts import render, get_object_or_404
from django.utils import timezone
from rest_framework.generics import (
    ListAPIView,
    RetrieveAPIView,
    CreateAPIView,
    UpdateAPIView,
    DestroyAPIView,
    RetrieveUpdateDestroyAPIView,
)
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.status import HTTP_200_OK, HTTP_400_BAD_REQUEST
from core.models import Item, OrderItem, Order
from .serializers import *
from core.models import *


class CustomerIDView(APIView):
    permission_classes = (IsAuthenticated,)

    def get(self, request, *args, **kwargs):
        return Response({"userID": request.user.id}, status=HTTP_200_OK)


# class CustomerItemView(ListAPIView):
#     # permission_classes = (IsAuthenticated,)
#     serializer_class = ItemSerializer
#     queryset = Item.objects.all()
#
#     def get_queryset(self):
#         category = self.request.query_params.get("category", None)
#         qs = Item.objects.all()
#
#         if category:
#             try:
#                 categoryobj = Category.objects.get(id=category)
#                 return qs.filter(category=categoryobj)
#             except ObjectDoesNotExist:
#                 raise Http404("You do not have an active order")
#
#         return qs

class ProductConfigView(ListAPIView):
    serializer_class = ProductConfigSerializer
    queryset = ProductConfig.objects.all()


class ProductVariationItemView(APIView):

    def get(self, request, format=None, *args, **kwargs):
        pk = kwargs.get('pk')
        size_id = request.GET.get("size_id", None)
        material_id = request.GET.get("material_id", None)
        qs = ProductItemsVariation.objects.filter(product_id=pk, material_id=material_id,
                             size_id=size_id).values('price', 'variant_Stock')
        return HttpResponse(json.dumps(list(qs), indent=4))


# class CustomerItemView(ListAPIView):
#     # permission_classes = (IsAuthenticated,)
#     serializer_class = ProductSerializer
#     queryset = Product.objects.all()
#
#     def get_queryset(self):
#         category = self.request.query_params.get("category", None)
#         qs = Product.objects.all()
#
#         if category:
#             try:
#                 categoryobj = Category.objects.get(slug=category)
#                 return qs.filter(category=categoryobj)
#             except ObjectDoesNotExist:
#                 raise Http404("You do not have an active order")
#
#         return qs

class CustomerItemView(APIView):
    # this function customize based upon category to get the product
    def get(self, request):
        category = self.request.query_params.get("category", None)
        data = {}
        prod_data = list()
        # prod_list = Product.objects.filter(categoory__slug='cat-test-demo-123')
        prod_list = Product.objects.all()
        if category:
            prod_list = prod_list.filter(Q(category__slug=category) | Q(category__parent__slug=category))
        for prod in prod_list:
            result = {}
            cat_data = {}
            parent_cat_data = {}
            if prod.category:
                cat_data['id'] = prod.category.id
                cat_data['category_items'] = prod.category.category_items
                cat_data['categoryName'] = prod.category.categoryName
            if prod.category.parent:
                parent_cat_data['id'] = prod.category.parent.id
                parent_cat_data['category_items'] = prod.category.parent.category_items
                parent_cat_data['categoryName'] = prod.category.parent.categoryName
            result['category'] = cat_data
            result['parent'] = parent_cat_data
            result['id'] = prod.id
            result['name'] = prod.name
            result['description'] = prod.description
            result['is_shop'] = prod.is_shop
            result['is_enquiry'] = prod.is_enquiry
            result['unitPrice'] = prod.unitPrice
            result['discountPrice'] = prod.discountPrice
            result['unitsInStock'] = prod.unitsInStock
            result['enquiry_text'] = prod.enquiry_text
            result['variation_stat'] = prod.variation_stat
            result['logo'] = str(prod.logo)
            result['slug'] = prod.slug
            images = list()
            images_list = ProductImages.objects.filter(product_id=prod.id)
            for img in images_list:
                img_data = {}
                img_data['id'] = img.id
                img_data['product'] = img.product.name
                img_data['product'] = img.product.name
                img_data['image'] = str(img.image)
                images.append(img_data)
            prod_data.append(result)
            result['images'] = images
            variation = list()
            variation_list = ProductItemsVariation.objects.filter(product_id=prod.id)
            for var in variation_list:
                var_data = {}
                size_data = {}
                material_data = {}
                var_data['id'] = var.id
                if var.material:
                    material_data['id'] = var.material.id
                    material_data['material_name'] = var.material.material_name
                if var.size:
                    size_data['id'] = var.size.id
                    size_data['size_name'] = var.size.size_name
                var_data['size'] = size_data
                var_data['material'] = material_data
                var_data['product'] = var.product.name
                var_data['price'] = var.price
                var_data['variant_Stock'] = var.variant_Stock
                var_data['default_material'] = var.default_material
                variation.append(var_data)
            result['variations'] = variation
        data['results'] = prod_data
        return JsonResponse(data, safe=False)



class CustomerItemDetailView(RetrieveAPIView):
    serializer_class = ProductSerializer
    queryset = Product.objects.all()
    lookup_field = 'slug'


class AddToCartView(APIView):
    def post(self, request, *args, **kwargs):
        slug = request.data.get("slug", None)
        variations = request.data.get("variations", [])
        quantity = request.data.get("quantity", None)
        if slug is None:
            return Response({"message": "Invalid request"}, status=HTTP_400_BAD_REQUEST)
        item = get_object_or_404(Item, slug=slug)

        minimun_variations_count = Variation.objects.filter(item=item).count()
        if len(variations) < minimun_variations_count:
            return Response(
                {"message": "Please specify the required variation types"},
                status=HTTP_400_BAD_REQUEST,
            )

        order_item_qs = OrderItem.objects.filter(
            item=item, user=request.user, ordered=False
        )

        for v in variations:
            order_item_qs = order_item_qs.filter(Q(item_variations__exact=v))

        if order_item_qs.exists():
            order_item = order_item_qs.first()
            order_item.quantity += quantity
            order_item.save()
        else:
            order_item = OrderItem.objects.create(
                item=item, user=request.user, ordered=False, quantity=quantity
            )
            order_item.item_variations.add(*variations)
            order_item.save()

        order_qs = Order.objects.filter(user=request.user, ordered=False)
        if order_qs.exists():
            order = order_qs[0]
            if not order.items.filter(item__id=order_item.id).exists():
                order.items.add(order_item)
            return Response(status=HTTP_200_OK)
        else:
            ordered_date = timezone.now()
            order = Order.objects.create(user=request.user, ordered_date=ordered_date)
            order.items.add(order_item)
            return Response(status=HTTP_200_OK)


class OrderQuantityUpdateView(APIView):
    def post(self, request, *args, **kwargs):
        slug = request.data.get("slug", None)
        if slug is None:
            return Response({"message": "Invalid Data"}, status=HTTP_400_BAD_REQUEST)
        item = get_object_or_404(Item, slug=slug)
        order_qs = Order.objects.filter(user=request.user, ordered=False)
        if order_qs.exists():
            order = order_qs[0]
            if order.items.filter(item__slug=item.slug).exists():
                order_item = OrderItem.objects.filter(
                    item=item, user=request.user, ordered=False
                )[0]
                if order_item.quantity > 1:
                    order_item.quantity -= 1
                    order_item.save()
                else:
                    order.items.remove(order_item)
                    Order.objects.get(user=request.user.id).delete()
                    OrderItem.objects.get(user=request.user.id).delete()
                return Response(status=HTTP_200_OK)
            else:
                return Response(
                    {"message": "This item was not in your cart"},
                    status=HTTP_400_BAD_REQUEST,
                )
        else:
            return Response(
                {"message": "You do not have an active order"},
                status=HTTP_400_BAD_REQUEST,
            )


class OrderItemDeleteView(DestroyAPIView):
    permission_classes = (IsAuthenticated,)
    queryset = OrderItem.objects.all()


class OrderDetailView(RetrieveAPIView):
    serializer_class = OrderSerializer
    permission_classes = (IsAuthenticated,)

    def get_object(self):
        try:
            order = Order.objects.get(user=self.request.user, ordered=False)
            return order
        except ObjectDoesNotExist:
            raise Http404("You do not have an active order")


class AddCouponView(APIView):
    def post(self, request, *args, **kwargs):
        code = request.data.get("code", None)
        if code is None:
            return Response(
                {"message": "Invalid data received"}, status=HTTP_400_BAD_REQUEST
            )
        order = Order.objects.get(user=self.request.user, ordered=False)
        coupon = get_object_or_404(Coupon, code=code)
        order.coupon = coupon
        order.save()
        return Response(status=HTTP_200_OK)


class AddressListView(ListAPIView):
    permission_classes = (IsAuthenticated,)
    serializer_class = AddressSerializer

    def get_queryset(self):
        address_type = self.request.query_params.get("address_type", None)
        qs = Address.objects.all()
        if address_type is None:
            return qs
        return qs.filter(user=self.request.user, address_type=address_type)


class AddressCreateView(CreateAPIView):
    permission_classes = (IsAuthenticated,)
    serializer_class = AddressSerializer
    queryset = Address.objects.all()


class AddressUpdateView(UpdateAPIView):
    permission_classes = (IsAuthenticated,)
    serializer_class = AddressSerializer
    queryset = Address.objects.all()


class AddressDeleteView(DestroyAPIView):
    permission_classes = (IsAuthenticated,)
    serializer_class = AddressSerializer
    queryset = Address.objects.all()
