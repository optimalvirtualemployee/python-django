from django_countries.serializer_fields import CountryField
from rest_framework import serializers
from core.models import *
from website_admin.models import *
from website_admin.models import ProductConfig
from website.serializers import *


class StringSerializer(serializers.ModelSerializer):
    def to_internal_value(self, value):
        return value


class CouponSerializer(serializers.ModelSerializer):
    class Meta:
        model = Coupon
        fields = ("id", "code", "amount")


class ItemSerializer(serializers.ModelSerializer):
    category = serializers.SerializerMethodField()
    images = serializers.SerializerMethodField()

    class Meta:
        model = Item
        fields = (
            "id",
            "productName",
            "productDescription",
            "category",
            "unitPrice",
            "discountPrice",
            "unitsInStock",
            "logo",
            "stl",
            "slug",
            "images",
        )

    def get_category(self, obj):
        return obj.category.categoryName

    def get_images(self, obj):
        return ItemImageSerializer(obj.itemimage_set.all(), many=True).data


class VariationDetailSerializer(serializers.ModelSerializer):
    item = serializers.SerializerMethodField()

    class Meta:
        model = Variation
        fields = ("id", "name", "item")

    def get_item(self, obj):
        return ItemSerializer(obj.item).data


class ItemVariationDetailSerializer(serializers.ModelSerializer):
    variation = serializers.SerializerMethodField()

    class Meta:
        model = ItemVariation
        fields = ("id", "value", "attachment", "variation")

    def get_variation(self, obj):
        return VariationDetailSerializer(obj.variation).data


class OrderItemSerializer(serializers.ModelSerializer):
    item_variations = serializers.SerializerMethodField()
    item = serializers.SerializerMethodField()
    final_price = serializers.SerializerMethodField()

    class Meta:
        model = OrderItem
        fields = ("id", "item", "item_variations", "quantity", "final_price")

    def get_item(self, obj):
        return ItemSerializer(obj.item).data

    def get_item_variations(self, obj):
        return ItemVariationDetailSerializer(obj.item_variations.all(), many=True).data

    def get_final_price(self, obj):
        return obj.get_final_price()


class OrderSerializer(serializers.ModelSerializer):
    order_items = serializers.SerializerMethodField()
    total = serializers.SerializerMethodField()
    coupon = serializers.SerializerMethodField()

    class Meta:
        model = Order
        fields = ("id", "order_items", "total", "coupon")
        # fields = '__all__'

    def get_order_items(self, obj):
        return OrderItemSerializer(obj.items.all(), many=True).data

    def get_total(self, obj):
        return obj.get_total()

    def get_coupon(self, obj):
        if obj.coupon is not None:
            return CouponSerializer(obj.coupon).data
        return None


class ItemVariationSerializer(serializers.ModelSerializer):
    class Meta:
        model = ItemVariation
        fields = ("id", "value", "attachment")


class VariationSerializer(serializers.ModelSerializer):
    item_variations = serializers.SerializerMethodField()

    class Meta:
        model = Variation
        fields = ("id", "name", "item_variations")

    def get_item_variations(self, obj):
        return ItemVariationSerializer(obj.itemvariation_set.all(), many=True).data


class ItemImageSerializer(serializers.ModelSerializer):
    class Meta:
        model = ItemImage
        fields = ("id", "image")


class ItemDetailSerializer(serializers.ModelSerializer):
    category = serializers.SerializerMethodField()
    variations = serializers.SerializerMethodField()
    images = serializers.SerializerMethodField()

    class Meta:
        model = Item
        fields = (
            "id",
            "productName",
            "productDescription",
            "category",
            "unitPrice",
            "discountPrice",
            "unitsInStock",
            "logo",
            "stl",
            "slug",
            "variations",
            "images",
        )

    def get_category(self, obj):
        return obj.category.categoryName

    def get_variations(self, obj):
        return VariationSerializer(obj.variation_set.all(), many=True).data

    def get_images(self, obj):
        return ItemImageSerializer(obj.itemimage_set.all(), many=True).data


class AddressSerializer(serializers.ModelSerializer):
    country = CountryField()

    class Meta:
        model = Address
        fields = (
            "id",
            "user",
            "street_address",
            "apartment_address",
            "country",
            "zip",
            "address_type",
            "default_addr",
        )


class SizeSerializer(serializers.ModelSerializer):
    class Meta:
        model = Size
        fields = ['id', 'size_name']


class MaterialSerializer(serializers.ModelSerializer):
    class Meta:
        model = Material
        fields = ['id', 'material_name']


class ProductItemsVariationSerializer(serializers.ModelSerializer):
    material = MaterialSerializer()
    size = SizeSerializer()
    product = serializers.StringRelatedField()

    class Meta:
        model = ProductItemsVariation
        exclude = ('prod_variation',)


class ProductImagesSerializer(serializers.ModelSerializer):
    product = serializers.StringRelatedField()

    class Meta:
        model = ProductImages
        fields = '__all__'


class ProductConfigSerializer(serializers.ModelSerializer):
    class Meta:
        model = ProductConfig
        fields = '__all__'


class ProductSerializer(serializers.ModelSerializer):
    # category = serializers.SerializerMethodField()
    # category = serializers.CharField(read_only=True, source="category.categoryName")
    # category = CategorySerializer(read_only=True)

    images = serializers.SerializerMethodField()
    variations = serializers.SerializerMethodField()

    class Meta:
        model = Product
        fields = (
            "category",
            "id",
            "name",
            "description",
            "is_shop",
            "is_enquiry",
            "unitPrice",
            "discountPrice",
            "unitsInStock",
            "enquiry_text",
            "variation_stat",
            "logo",
            "slug",
            "images",
            "variations",
        )

    # def get_category(self, obj):
    #     return obj.category.categoryName

    def get_variations(self, obj):
        return ProductItemsVariationSerializer(obj.product_productitemsvariations.all(), many=True).data

    def get_images(self, obj):
        return ProductImagesSerializer(obj.product_productimagess.all(), many=True).data
