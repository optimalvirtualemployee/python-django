from django.urls import path, include
from .views import *

urlpatterns = [
    path("id/", CustomerIDView.as_view(), name="customer_id"),
    path("products/", CustomerItemView.as_view(), name="products"),
    path("get_product_variation/<pk>/", ProductVariationItemView.as_view(), name="get_product_variation"),
    path("product_config/", ProductConfigView.as_view(), name="product_config"),
    path("add-to-cart/", AddToCartView.as_view(), name="add-to-cart"),
    path(
        "product-details/<slug>/",
        CustomerItemDetailView.as_view(),
        name="product_details",
    ),
    path("cart/", OrderDetailView.as_view(), name="cart"),
    path("add-coupon/", AddCouponView.as_view(), name="add_coupon"),
    path("addresses/", AddressListView.as_view(), name="addresses"),
    path("address/create/", AddressCreateView.as_view(), name="address_create"),
    path("address/<pk>/update/", AddressUpdateView.as_view(), name="address_update"),
    path("address/<pk>/delete/", AddressDeleteView.as_view(), name="address_delete"),
    path("order-item/<pk>/delete/", OrderItemDeleteView.as_view(), name="delete_view"),
    path(
        "order-item/update-quantity/",
        OrderQuantityUpdateView.as_view(),
        name="update-quantity",
    ),
]
