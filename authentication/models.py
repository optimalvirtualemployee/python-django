from django.db import models
from django.contrib.auth.models import (
    AbstractBaseUser,
    BaseUserManager,
    PermissionsMixin,
)
from rest_framework_simplejwt.tokens import RefreshToken


class UserManager(BaseUserManager):
    def create_user(
        self, name, email, phone_number, role, company, city, street, password=None
    ):
        if email is None:
            raise TypeError("Users should have a username")

        user = self.model(name=name, email=email)
        user.set_password(password)
        user.save()
        return user

    def create_superuser(
        self, name, email, phone_number, role, company, city, street, password=None
    ):
        if password is None:
            raise TypeError("Password should not be none")

        user = self.create_user(
            name, email, phone_number, role, company, city, street, password
        )
        user.is_superuser = True
        user.is_staff = True
        user.save()
        return user


AUTH_PROVIDERS = {"facebook": "facebook", "google": "google", "email": "email"}

role = (("customer", "CUSTOMER"), ("admin", "ADMIN"), ("worker", "WORKER"))


class User(AbstractBaseUser, PermissionsMixin):
    name = models.CharField(max_length=255)
    email = models.CharField(max_length=255, unique=True, db_index=True)
    phone_number = models.CharField(
        max_length=50, unique=True, db_index=True, blank=True, null=True
    )
    role = models.CharField(choices=role, max_length=50, default="customer")
    is_active = models.BooleanField(default=True)
    is_staff = models.BooleanField(default=False)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now_add=True)
    auth_provider = models.CharField(
        max_length=255, blank=False, null=False, default=AUTH_PROVIDERS.get("email")
    )

    company = models.CharField(max_length=255, blank=False, null=False, default="None")
    city = models.CharField(max_length=255, blank=False, null=False, default="None")
    street = models.CharField(max_length=255, blank=False, null=False, default="None")

    USERNAME_FIELD = "email"
    REQUIRED_FIELDS = ["name", "phone_number", "role", "company", "city", "street"]

    objects = UserManager()

    def __str__(self):
        return self.email

    def tokens(self):
        refresh = RefreshToken.for_user(self)
        return {"refresh": str(refresh), "access": str(refresh.access_token)}
