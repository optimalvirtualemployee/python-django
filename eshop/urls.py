from django.conf.urls import url
from django.contrib import admin
from django.urls import path, include
from rest_framework import permissions
from drf_yasg.views import get_schema_view
from drf_yasg import openapi
from django.views.static import serve
from django.conf import settings

schema_view = get_schema_view(
    openapi.Info(
        title="ESHOP API",
        default_version="v1",
        description="Test description",
    ),
    public=True,
    permission_classes=(permissions.AllowAny,),
)

urlpatterns = [
    #### Django SuperAdmin
    url(r"^superadmin/", admin.site.urls),
    #### API's
    path("auth/", include("authentication.urls")),
    path("social_auth/", include("social_auth.urls")),
    path("eadmin/", include("adminpanal.urls")),
    path("customer/", include("customer.urls")),
    path("website/", include("website.urls")),
    #### Subadmin Template
    path("admin/", include("website_admin.urls")),
    path("admin/shipping/", include("shipping.urls")),
    #### Swagger
    path("", schema_view.with_ui("swagger", cache_timeout=0), name="schema-swagger-ui"),
    url(r"^media/(?P<path>.*)$", serve, {"document_root": settings.MEDIA_ROOT}),
    path("redoc/", schema_view.with_ui("redoc", cache_timeout=0), name="schema-redoc"),
]
