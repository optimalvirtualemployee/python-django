from django.apps import AppConfig


class SocalAuthConfig(AppConfig):
    name = "social_auth"
