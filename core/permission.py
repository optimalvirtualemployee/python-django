from rest_framework import permissions


class IsOwner(permissions.BasePermission):
    def has_object_permission(self, request, view, obj):
        return obj.user == request.user


def _is_in_group(user, group_name):
    if user == group_name:
        return user
    else:
        return None


def _has_group_permission(user, required_groups):
    return any([_is_in_group(user, group_name) for group_name in required_groups])


class IsAdminUser(permissions.BasePermission):

    required_groups = ["admin"]

    def has_permission(self, request, view):
        has_group_permission = _has_group_permission(
            request.user.role, self.required_groups
        )
        return request.user and has_group_permission

    def has_object_permission(self, request, view, obj):
        has_group_permission = _has_group_permission(
            request.user.role, self.required_groups
        )
        return request.user and has_group_permission


class IsWorkerUser(permissions.BasePermission):
    required_groups = ["worker"]

    def has_permission(self, request, view):
        has_group_permission = _has_group_permission(
            request.user.userrole.name, self.required_groups
        )
        return request.user and has_group_permission

    def has_object_permission(self, request, view, obj):
        has_group_permission = _has_group_permission(
            request.user.userrole.name, self.required_groups
        )
        return request.user and has_group_permission
