# Generated by Django 3.1.4 on 2021-01-15 06:43

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("core", "0010_auto_20210115_1210"),
    ]

    operations = [
        migrations.AlterField(
            model_name="category",
            name="logo",
            field=models.CharField(max_length=255),
        ),
        migrations.AlterField(
            model_name="itemvariation",
            name="attachment",
            field=models.CharField(blank=True, max_length=255, null=True),
        ),
    ]
