# Generated by Django 3.1.7 on 2021-04-05 15:24

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0037_auto_20210405_2039'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='service',
            name='service_description',
        ),
        migrations.AddField(
            model_name='service',
            name='description',
            field=models.TextField(default=''),
        ),
    ]
