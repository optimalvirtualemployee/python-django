# Generated by Django 3.1.4 on 2021-01-11 07:47

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ("core", "0007_remove_orderitem_variations_time"),
    ]

    operations = [
        migrations.AddField(
            model_name="orderitem",
            name="variation",
            field=models.ForeignKey(
                blank=True,
                null=True,
                on_delete=django.db.models.deletion.CASCADE,
                to="core.variation",
            ),
        ),
    ]
