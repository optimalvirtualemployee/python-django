# Generated by Django 3.1.7 on 2021-04-05 16:35

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0039_auto_20210405_2054'),
    ]

    operations = [
        migrations.AlterField(
            model_name='homeslider',
            name='description',
            field=models.TextField(default=''),
        ),
    ]
