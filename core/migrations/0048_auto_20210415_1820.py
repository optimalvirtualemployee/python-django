# Generated by Django 3.1.7 on 2021-04-15 12:50

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0047_auto_20210415_1815'),
    ]

    operations = [
        migrations.RenameField(
            model_name='banners',
            old_name='banner_description',
            new_name='short_description',
        ),
    ]
