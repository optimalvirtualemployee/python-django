from django.db import models
from django.conf import settings
from django_countries.fields import CountryField
from authentication.models import User
from django.utils.text import slugify

length = 255


class Category(models.Model):
    CATEGORY = (
        ("EP", "Enterprises Product"),
        ("CP", "Consumer Product"),
        ("DS", "Design Services"),
    )
    length = 255
    category_items = models.CharField(max_length=length, choices=CATEGORY, default="EP")
    categoryName = models.CharField(max_length=length)
    parent = models.ForeignKey('self', on_delete=models.CASCADE, null=True, blank=True,
                                        related_name="child_category")
    description = models.TextField(default='')
    logo = models.ImageField()
    is_active = models.BooleanField(default=True)
    slug = models.SlugField(max_length=255, null=True, blank=True, unique=True)

    def __str__(self):
        return self.categoryName

    def save(self, *args, **kwargs):
        self.slug = slugify(self.categoryName, allow_unicode=True)
        super(Category, self).save(*args, **kwargs)


class SubCategory(models.Model):
    CATEGORY = (
        ("EP", "Enterprises Product"),
        ("CP", "Consumer Product"),
        ("DS", "Design Services"),
    )
    length = 255
    category_items = models.CharField(max_length=length, choices=CATEGORY, default="EP")
    categoryName = models.CharField(max_length=length)
    parent_category = models.ForeignKey(Category, on_delete=models.CASCADE, null=True, blank=True,
                                        related_name="sub_parent_%(class)ss")
    description = models.TextField(default='')
    logo = models.ImageField()
    is_active = models.BooleanField(default=True)

    def __str__(self):
        return self.categoryName


class Item(models.Model):
    length = 255
    productName = models.CharField(max_length=length)
    productDescription = models.TextField(default='')
    category = models.ForeignKey(
        Category, related_name="category", on_delete=models.CASCADE
    )
    unitPrice = models.IntegerField()
    discountPrice = models.FloatField(blank=True, null=True)
    unitsInStock = models.CharField(max_length=length)
    logo = models.ImageField(blank=True, null=True)
    stl = models.FileField(blank=True, null=True)
    slug = models.CharField(max_length=length, unique=True)

    def __str__(self):
        return self.productName

    def get_absolute_url(self):
        return reverse("core:product", kwargs={"slug": self.slug})

    def get_add_to_cart_url(self):
        return reverse("core:add-to-cart", kwargs={"slug": self.slug})

    def get_remove_from_cart_url(self):
        return reverse("core:remove-from-cart", kwargs={"slug": self.slug})

    @property
    def image(self):
        return self.itemimage_set.all()


class ItemImage(models.Model):
    length = 255
    item = models.ForeignKey(Item, on_delete=models.CASCADE)
    image = models.ImageField()  # image

    def __str__(self):
        return self.item.productName


class Variation(models.Model):
    item = models.ForeignKey(Item, on_delete=models.CASCADE)
    name = models.CharField(max_length=50)  # material
    unitPrise = models.IntegerField(blank=True, null=True)

    class Meta:
        unique_together = ("item", "name")

    def __str__(self):
        return self.name


class ItemVariation(models.Model):
    variation = models.ForeignKey(Variation, on_delete=models.CASCADE)
    value = models.CharField(max_length=50)  # size
    attachment = models.CharField(blank=True, null=True, max_length=255)

    class Meta:
        unique_together = ("variation", "value")

    def __str__(self):
        return self.value


class OrderItem(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    variation = models.ForeignKey(
        Variation, on_delete=models.CASCADE, blank=True, null=True
    )
    ordered = models.BooleanField(default=False)
    item = models.ForeignKey(Item, on_delete=models.CASCADE, null=True, blank=True)
    item_variations = models.ManyToManyField(ItemVariation)
    quantity = models.IntegerField(default=1)

    def __str__(self):
        return f"{self.quantity} of {self.item.productName}"

    def get_total_item_price(self):
        return self.quantity * self.item.unitPrice

    def get_total_discount_item_price(self):
        return self.quantity * self.item.unitPrice

    def get_amount_saved(self):
        return self.get_total_item_price() - self.get_total_discount_item_price()

    def get_final_price(self):
        if self.item.discountPrice:
            return self.get_total_discount_item_price()
        return self.get_total_item_price()


class Order(models.Model):
    length = 255
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, null=True, blank=True)
    ref_code = models.CharField(max_length=length, blank=True, null=True)
    items = models.ManyToManyField(OrderItem, null=True, blank=True)
    start_date = models.DateTimeField(auto_now_add=True)
    ordered_date = models.DateTimeField()
    ordered = models.BooleanField(default=False)
    shipping_address = models.ForeignKey(
        "Address",
        related_name="shipping_address",
        on_delete=models.SET_NULL,
        blank=True,
        null=True,
    )
    billing_address = models.ForeignKey(
        "Address",
        related_name="billing_address",
        on_delete=models.SET_NULL,
        blank=True,
        null=True,
    )
    payment = models.ForeignKey(
        "Payment",
        related_name="payment",
        on_delete=models.SET_NULL,
        blank=True,
        null=True,
    )
    coupon = models.ForeignKey(
        "Coupon",
        related_name="coupon",
        on_delete=models.SET_NULL,
        blank=True,
        null=True,
    )
    being_delivered = models.BooleanField(default=False)
    refund_requested = models.BooleanField(default=False)
    refund_granted = models.BooleanField(default=False)
    tracking_id = models.CharField(max_length=255, null=True, blank=True)

    def __str__(self):
        return self.user.email

    def get_total(self):
        total = 0
        for order_item in self.items.all():
            total += order_item.get_final_price()
        if self.coupon:
            total -= self.coupon.amount
        return total


class Address(models.Model):
    ADDRESS_CHOICES = (
        ("B", "Billing"),
        ("S", "Shipping"),
    )
    length = 255
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    street_address = models.CharField(max_length=length)
    apartment_address = models.CharField(max_length=length)
    country = CountryField(multiple=False)
    zip = models.CharField(max_length=length)
    address_type = models.CharField(max_length=length, choices=ADDRESS_CHOICES)
    default_addr = models.BooleanField(default=False)

    def __str__(self):
        return self.user.email

    class Meta:
        verbose_name_plural = "Addresses"


class Payment(models.Model):
    stripe_charge_id = models.CharField(max_length=50)
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    amount = models.FloatField()
    timestamp = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.user.email


class Coupon(models.Model):
    code = models.CharField(max_length=15)
    amount = models.FloatField()
    is_active = models.BooleanField(default=True)

    def __str__(self):
        return self.code


class HomeContent(models.Model):
    title = models.CharField(max_length=length)
    description = models.CharField(max_length=500, blank=True, null=True)
    slider_image1 = models.FileField(blank=True, null=True)
    slider_image2 = models.ImageField(blank=True, null=True)
    slider_image3 = models.ImageField(blank=True, null=True)
    service_heading = models.CharField(max_length=length, blank=True, null=True)
    service_description = models.CharField(max_length=length, blank=True, null=True)

    def __str__(self):
        return self.title


class ImageImage(models.Model):
    home_content = models.ForeignKey(
        HomeContent, related_name="home_contemt", on_delete=models.CASCADE
    )
    title = models.CharField(max_length=244, blank=True, null=True)
    description = models.CharField(max_length=244, blank=True, null=True)
    image = models.ImageField()  # image

    def __str__(self):
        return self.name


class Service(models.Model):
    service_name = models.CharField(max_length=length)
    image = models.ImageField()
    service_description = models.TextField(default='')
    short_description = models.TextField(default='', null=True, blank=True)
    link = models.CharField(max_length=244, default='')

    def __str__(self):
        return self.service_name


class Client(models.Model):
    client_name = models.CharField(max_length=length)
    image = models.ImageField()
    client_description = models.CharField(max_length=length)

    def __str__(self):
        return self.client_name


class HomeSlider(models.Model):
    title = models.CharField(max_length=244, default='')
    description = models.TextField(default='')
    image = models.ImageField()  # image
    link = models.CharField(max_length=244, default='')

    def __str__(self):
        return self.title


class Banners(models.Model):
    title = models.CharField(max_length=244, default='')
    short_description = models.TextField(default='')
    image = models.ImageField()  # image
    link = models.CharField(max_length=244, default='')

    def __str__(self):
        return self.title


class Website(models.Model):
    logo = models.ImageField()  # image
    url = models.CharField(max_length=244, blank=True, null=True)
    title = models.CharField(max_length=244, blank=True, null=True)
    meta_description = models.CharField(max_length=244, blank=True, null=True)

    def __str__(self):
        return self.title


class Logo(models.Model):
    title = models.CharField(max_length=244, default='')
    logo = models.ImageField()  # image
    description = models.TextField(default='')
    is_active = models.BooleanField(default=True)


class EmailSubscriber(models.Model):
    email = models.EmailField(max_length=255, null=True, blank=True)
    is_active = models.BooleanField(default=True)
    created_at = models.DateTimeField(auto_now=True, null=True, blank=True)
    updated_at = models.DateTimeField(auto_now=True, null=True, blank=True)

    def __str__(self):
        return self.email


class ProductEnquiry(models.Model):
    name = models.CharField(max_length=255, null=True, blank=True)
    number = models.CharField(max_length=255, null=True, blank=True)
    email = models.EmailField(max_length=255, null=True, blank=True)
    file = models.TextField(null=True, blank=True)
    is_active = models.BooleanField(default=True)
    created_at = models.DateTimeField(auto_now=True, null=True, blank=True)
    updated_at = models.DateTimeField(auto_now=True, null=True, blank=True)

    def __str__(self):
        return self.email


class ContactEnquiry(models.Model):
    name = models.CharField(max_length=255, null=True, blank=True)
    phone_number = models.CharField(max_length=255, null=True, blank=True)
    email = models.EmailField(max_length=255, null=True, blank=True)
    subject = models.CharField(max_length=255, null=True, blank=True)
    message = models.TextField(null=True, blank=True)
    is_active = models.BooleanField(default=True)
    created_at = models.DateTimeField(auto_now=True, null=True, blank=True)
    updated_at = models.DateTimeField(auto_now=True, null=True, blank=True)

    def __str__(self):
        return self.email


class Country(models.Model):
    name = models.CharField(max_length=150)
    country_code = models.CharField(max_length=50)
    status = models.BooleanField(default=True)
    created_at = models.DateTimeField(auto_now_add=True, null=True, blank=True)
    updated_at = models.DateTimeField(auto_now=True, null=True, blank=True)

    def __str__(self):
        return self.name
